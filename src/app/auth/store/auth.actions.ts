import { createAction, props } from "@ngrx/store";
import { AuthError, AuthResponse, LoginPayload, SignupPayload } from "./auth.interfaces";

export const handleLogin = createAction('[Auth] Login' , props<{payload : LoginPayload}>());

export const handleSignUp = createAction('[Auth] Signup' , props<{payload : SignupPayload}>());

export const setLoading = createAction('[Auth] Set Loading' , props<{loading : boolean}>());

export const handleFailure = createAction('Error' , props<{error : AuthError}>());

export const handleSuccess = createAction('Response' , props<{response : AuthResponse}>());

export const handleLogout = createAction('[Auth] Logout');

export const resetAuthState = createAction('[Auth] Reset State');

