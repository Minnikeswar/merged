import { createReducer, on } from "@ngrx/store";
import { handleFailure, handleLogout, handleSuccess, resetAuthState, setLoading } from './auth.actions'
import { AuthError, AuthErrorInit, AuthResponse, AuthResponseInit } from "./auth.interfaces";

export const isLoading : number = 0;
export const error : AuthError = AuthErrorInit;
export const response : AuthResponse = AuthResponseInit;

export const loadingReducer = createReducer(
    isLoading,
    on(setLoading , (state , {loading}) => {
        if(loading) return state + 1;
        return state - 1;
    }),
    on(resetAuthState , (state) => 0),
    on(handleLogout , (state) => 0)
)

export const errorReducer = createReducer(
    error,
    on(handleFailure , (state , {error}) => error),
    on(handleLogout , (state) => AuthErrorInit),
    on(resetAuthState , (state) => AuthErrorInit)
)

export const responseReducer = createReducer(
    response,
    on(handleSuccess , (state , {response}) => {
        return response;
    }),
    on(handleLogout , (state) => AuthResponseInit),
    on(resetAuthState , (state) => AuthResponseInit),
)

