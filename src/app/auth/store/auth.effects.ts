import { Injectable } from "@angular/core";
import { Actions, ofType, createEffect } from "@ngrx/effects";
import { AuthService } from "./auth.service";
import { handleLogin, handleSignUp } from "./auth.actions";
import { handleFailure, handleSuccess } from "./auth.actions";
import { tap } from "rxjs";
import { catchError, exhaustMap, finalize, map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { of } from "rxjs";
import { AuthError, AuthResponse } from "./auth.interfaces";

@Injectable()
export class AuthEffects {
    constructor(private actions$: Actions, private authService: AuthService, private http: HttpClient) { }

    handleLogin = createEffect(() =>
        this.actions$.pipe(
            ofType(handleLogin),
            tap(() => this.authService.setLoading(true)),
            exhaustMap(action => {
                return this.authService.login(action.payload).pipe(
                    map((data: AuthResponse) => {
                        data.responseFor = 'login';
                        return handleSuccess({ response: data });
                    }),
                    catchError((err : AuthError) => {
                        return of(handleFailure({ error: err }));
                    }),
                    finalize(() => this.authService.setLoading(false))
                )
            }),
        ))

    handleSignUp = createEffect(() =>
        this.actions$.pipe(
            ofType(handleSignUp),
            tap(() => this.authService.setLoading(true)),
            exhaustMap(action => {
                return this.authService.register(action.payload).pipe(
                    map((data: string) => {
                        const response : AuthResponse = {
                            message : data,
                            responseFor : 'signup'
                        }
                        return handleSuccess({ response });
                    }),
                    catchError((err : AuthError) => {
                        return of(handleFailure({ error: err }));
                    }),
                    finalize(() => this.authService.setLoading(false))
                )
            }),
        )
    )
}