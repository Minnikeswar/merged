import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { Store } from '@ngrx/store';
import { handleFailure, handleSuccess, setLoading } from './auth.actions';
import { catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthError, AuthResponse, ForgotPasswordPayload, LoginPayload, ResetPasswordPayload, SignupPayload } from './auth.interfaces';
import { AppCookieService } from 'src/app/recruiter/store/cookie.service';
import { UserData } from 'src/app/recruiter/store/recruiter.interfaces';
import { response } from './auth.reducers';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private store: Store , private appCookieService: AppCookieService) {

  }

  setLoading(loading: boolean) : void {
    this.store.dispatch(setLoading({ loading }));
  }

  login(payload: LoginPayload): Observable<AuthResponse> {
    return this.http.post(environment.backendUrl + '/auth/login', payload).pipe(
      map((response) => response as AuthResponse)
    )
  }

  register(payload: SignupPayload): Observable<string> {
    return this.http.post(environment.backendUrl + '/auth/register', payload, { responseType: 'text' });
  }

  forgotPassword(email: ForgotPasswordPayload): Observable<string> {
    this.setLoading(true);
    return this.http.post(environment.backendUrl + '/auth/resetpassword', { email }, { responseType: 'text' }).pipe(
      map((response) => {
        this.store.dispatch(handleSuccess({ response: { message: response } }));
        return 'SUCCESS'
      }),
      catchError((error) => {
        this.store.dispatch(handleFailure({ error }));
        return of('ERROR')
      }),
      finalize(() => this.setLoading(false))
    )
  }

  resetPassword(payload: ResetPasswordPayload): Observable<string> {
    this.setLoading(true);
    return this.http.post(environment.backendUrl + '/auth/changepassword', payload, { responseType: 'text' }).pipe(
      map((response) => {
        this.store.dispatch(handleSuccess({ response: { message: response } }));
        return 'SUCCESS'
      }),
      catchError((error) => {
        this.store.dispatch(handleFailure({ error }));
        return of('ERROR')
      }),
      finalize(() => this.setLoading(false))
    )
  }

  /**
   * 
   * @param token 
   * @returns response from the server containing the deparsed jwt token, incase of error, it returns an object containing 404 status code
   */
  verifyJwt(token: string): Observable<string> {
    this.setLoading(true)
    return this.http.post(environment.backendUrl + '/auth/verifyjwt', { jwt: token }).pipe(
      map((response ) => {
        this.appCookieService.setUserData(response as UserData)
        return 'SUCCESS';
      }),
      catchError((error) => {
        this.store.dispatch(handleFailure({ error }));
        return of('ERROR')
      }),
      finalize(() => this.setLoading(false))
    )
  }
}
