
export interface LoginPayload {
    email : string;
    password : string;
}

export interface SignupPayload {
    email : string;
    password : string;
    username : string;
    confirmPassword : string;
    isRecruiter : boolean;
}

export interface ResetPasswordPayload {
    email : string;
    password : string;
    confirmPassword : string;
    otp : string;
}

export interface ForgotPasswordPayload{
    email : string;
}

export interface AuthResponse{
    responseFor ?: string;
    message : string;
    token ?: string;
}

export const AuthResponseInit : AuthResponse = {
    responseFor : '',
    message : '',
    token : ''
}

export interface AuthError{
    error ?: string;
}

export const AuthErrorInit : AuthError = {
    error : ''
}

export interface AuthState {
    loading : number;
    error : AuthError;
    response : AuthResponse;
}