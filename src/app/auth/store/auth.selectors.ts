import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from './auth.interfaces';

const authFeatureSelector = createFeatureSelector<AuthState>('auth');

export const loadingSelector = createSelector(
    authFeatureSelector,
    (state : AuthState) => state.loading
);

export const errorSelector = createSelector(
    authFeatureSelector,
    (state : AuthState) => state.error
);

export const responseSelector = createSelector(
    authFeatureSelector,
    (state : AuthState) => state.response
);