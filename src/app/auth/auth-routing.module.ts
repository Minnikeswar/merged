import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EntryComponent } from './entry/entry.component';
import { AuthComponent } from './auth.component';
import { isLoggedOut } from './guards/auth.guard';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const routes: Routes = [
  {path : '' , component : AuthComponent , children : [
    {path : '' , redirectTo : 'entry' , pathMatch : 'full'},
    {path : 'entry' , component : EntryComponent },
    {path : 'forgotPassword' , component : ForgotPasswordComponent},
    {path : 'resetPassword/:otp' , component : ResetPasswordComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
