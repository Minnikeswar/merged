import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { responseSelector, errorSelector, loadingSelector } from "./store/auth.selectors";
import { MessageService } from "primeng/api";
import { Observable, Subject, Subscription, takeUntil } from "rxjs";
import { AuthError, AuthErrorInit, AuthResponse, AuthResponseInit } from "./store/auth.interfaces";
import { resetAuthState } from "./store/auth.actions";

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
})

export class AuthComponent {
    responseSubscription: Subscription | null = null;
    errorSubscription: Subscription | null = null;

    destroySubject = new Subject<void>();

    isLoading$: Observable<number> = this.store.select(loadingSelector);
    constructor(private store: Store, private messageService: MessageService) {
        this.store.select(responseSelector).pipe(
            takeUntil(this.destroySubject)
        ).subscribe((response: AuthResponse) => {
            if (response === AuthResponseInit) return;
            this.messageService.clear();
            this.messageService.add({ severity: 'success', summary: 'Success', detail: response.message });
        })

        this.store.select(errorSelector).pipe(
            takeUntil(this.destroySubject)
        ).subscribe((resp: AuthError) => {
            if (resp === AuthErrorInit) return;
            this.messageService.clear();
            this.messageService.add({ severity: 'error', summary: 'Error', detail: resp.error ?? 'Error Occured at server' });
        })
    }

    ngOnDestroy() {
        this.destroySubject.next();
        this.destroySubject.complete();
        this.store.dispatch(resetAuthState());
    }
}