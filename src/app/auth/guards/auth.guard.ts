import { inject } from "@angular/core";
import { CanActivateFn, Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { AppCookieService } from "src/app/recruiter/store/cookie.service";

export const isLoggedOut : CanActivateFn = (route , state) => {
    const appCookieService = inject(AppCookieService);
    const router = inject(Router);
    const messageService = inject(MessageService);
    const token : string = appCookieService.getJwtToken();
    if(token){
        router.navigate(['rec/job']);
        return false;
    }
    return true;
}