import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../store/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ForgotPasswordPayload } from '../store/auth.interfaces';
import { emailValidator } from 'src/app/recruiter/validators/form.validators';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {

  subscription : Subscription | null = null;

  emailForm : FormGroup;
  constructor(private authService: AuthService , private router : Router) { 
    this.emailForm = new FormGroup({
      email : new FormControl('', [Validators.required, emailValidator])
    });
  }

  formSubmit(){
    const payload : ForgotPasswordPayload = this.emailForm.value;
    this.subscription = this.authService.forgotPassword(payload).subscribe((response : string) => {
      if(response === 'SUCCESS'){
        this.emailForm.reset();
        this.router.navigate(['/']);
      }
    });
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }
}
