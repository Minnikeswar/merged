import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../store/auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Subject, Subscription, takeUntil } from 'rxjs';
import { ResetPasswordPayload } from '../store/auth.interfaces';
import { emailValidator, passwordValidator } from 'src/app/recruiter/validators/form.validators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {

  passwordForm : FormGroup;
  otp : string = '';

  destroySubject = new Subject<void>();

  constructor(private authService: AuthService , private router : Router, private activatedRoute : ActivatedRoute) { 
    this.passwordForm = new FormGroup({
      email : new FormControl('', [Validators.required, emailValidator]),
      password : new FormControl('', [Validators.required , passwordValidator]),
      confirmPassword : new FormControl('', [Validators.required , passwordValidator])
    });
    this.activatedRoute.params.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((params) => {
      if(!params['otp']) return;
      this.otp = params['otp'];
    });
  }

  formSubmit(){
    const payload : ResetPasswordPayload = {
      ...this.passwordForm.value,
      otp : this.otp
    };
    this.authService.resetPassword(payload)
    .pipe(
      takeUntil(this.destroySubject)
    )
    .subscribe((response : string) => {
      if(response === 'SUCCESS'){
        this.passwordForm.reset();
        this.router.navigate(['/']);
      }
    });
  }

  ngOnDestroy(){
    this.destroySubject.next();
    this.destroySubject.complete();
  }
}
