import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { EntryComponent } from './entry/entry.component';
import { StoreModule } from '@ngrx/store';
import { errorReducer, loadingReducer, responseReducer } from './store/auth.reducers';
import { SharedModule } from '../shared/shared.module';
import { AuthComponent } from './auth.component';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './store/auth.effects';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';


@NgModule({
  declarations: [
    EntryComponent,
    AuthComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,

  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    StoreModule.forFeature('auth', {loading : loadingReducer , error : errorReducer , response : responseReducer}),
    EffectsModule.forFeature([AuthEffects]),
  ],
  providers:[]
})
export class AuthModule { }
