import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { handleLogin, handleSignUp } from '../store/auth.actions';
import { Router } from '@angular/router';
import { responseSelector } from '../store/auth.selectors';
import { AppCookieService } from 'src/app/recruiter/store/cookie.service';
import { AuthService } from '../store/auth.service';
import { Subject, Subscription, exhaustMap, takeUntil } from 'rxjs';
import { SignupPayload, LoginPayload, AuthResponse } from '../store/auth.interfaces';
import { emailValidator, passwordValidator } from 'src/app/recruiter/validators/form.validators';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss']
})
export class EntryComponent {

  isLoginActive: boolean;
  loginForm: FormGroup;
  signupForm: FormGroup;

  imgUrl1: string;
  imgUrl2: string;

  tokenSubject = new Subject<string>();
  destroySubject = new Subject<void>();

  constructor(private router: Router, private store: Store, private appCookieService: AppCookieService, private authService: AuthService) {
    this.imgUrl1 = '../../assets/images/entry_page.png';
    this.imgUrl2 = '../../assets/images/signup_page.png';

    this.isLoginActive = true;

    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email, emailValidator]),
      password: new FormControl('', [Validators.required, Validators.minLength(8), passwordValidator])
    })

    this.signupForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email, emailValidator]),
      password: new FormControl('', [Validators.required, Validators.minLength(8), passwordValidator]),
      confirmPassword: new FormControl('', [Validators.required, Validators.minLength(8), passwordValidator]),
      isRecruiter: new FormControl(true)
    })

    this.store.select(responseSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((resp: AuthResponse) => {
        if (!resp.responseFor) return;
        if (resp.responseFor === 'login') {
          if (!resp.token) return;
          const token: string = resp.token;
          this.appCookieService.setJwtToken(token);
          this.loginForm.reset();
          this.tokenSubject.next(token);
        }
        else if (resp.responseFor === 'signup') {
          this.signupForm.reset();
          this.toggleLogin();
        }
      });

    this.tokenSubject.pipe(
      takeUntil(this.destroySubject),
      exhaustMap((token: string) => this.authService.verifyJwt(token))
    ).subscribe((resp: string) => {
      if (resp === 'ERROR') {
        this.appCookieService.deleteJwtToken();
        return;
      }
      setTimeout(() => {
        this.router.navigate(['rec/job']);
      }, 100)
    });
  }

  toggleLogin(): void {
    setTimeout(() => {
      this.isLoginActive = !this.isLoginActive;
    }, 300);
  }

  signUp(): void {
    this.loginForm.reset();
    const payload: SignupPayload = this.signupForm.value;
    this.store.dispatch(handleSignUp({ payload }));
  }

  login(): void {
    this.signupForm.reset();
    const payload: LoginPayload = this.loginForm.value;
    this.store.dispatch(handleLogin({ payload }));
  }

  ngOnDestroy() {
    this.destroySubject.next();
    this.destroySubject.complete();
  }
}
