import { RouterModule, Routes } from "@angular/router";
import { InvitationsComponent } from "./invitations/invitations.component";
import { NgModule } from "@angular/core";
import { InvitesComponent } from "./invites.component";


const routes: Routes = [
    {
        path: '',
        component: InvitesComponent,
        children: [
           {
            path: 'all',
            component: InvitationsComponent,
           }
        ]
    }
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class InvitationsRoutingModule {}