import { createAction, props } from "@ngrx/store";
import { EachInvitation } from "../models/eachinvitation.model";

export const getInvitations = createAction('[invitations] myInvitations')

export const invitationSuccess = createAction('[invitation Success] myInvites',props<{data: EachInvitation[]}>())

export const invitationFailure = createAction('[invitation Failure] myInvites',props<{err: string}>())

