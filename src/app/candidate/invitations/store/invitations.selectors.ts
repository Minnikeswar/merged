import { createFeatureSelector, createSelector } from "@ngrx/store";
import { InvitationState } from "./invitations.reducers";


const featureState = createFeatureSelector<InvitationState>('invitations')

export const allInvitations = createSelector(featureState,state => state.data)