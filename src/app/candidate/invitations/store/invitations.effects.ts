import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { environment } from "src/environments/environment.development";
import { getInvitations, invitationFailure, invitationSuccess } from "./invitations.actions";
import { catchError, map, of, switchMap } from "rxjs";



@Injectable()
export class InviteEffect{
    actions$ = inject(Actions)
    url = environment.invitationsUrl
    constructor(private http: HttpClient){}

    invitesRequest$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(getInvitations),
            switchMap(() => {
                return this.http.get(this.url).pipe(map((data:any) => {
                    return invitationSuccess({data: data})
                })
                
                ,
                catchError((err) => {
                    return of(invitationFailure({err}));
                })
                )
            })
        )
    })
}