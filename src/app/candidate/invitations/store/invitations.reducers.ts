import { EachInvitation } from './../models/eachinvitation.model';

import { createReducer, on } from "@ngrx/store";
import { getInvitations, invitationFailure, invitationSuccess } from './invitations.actions';

export interface InvitationState {
    data: EachInvitation[];
    error: string;
}


const initialState:InvitationState = {
    data: [],
    error: '',
}

export const invitationsReducer = createReducer(
    initialState,
    on(getInvitations,(state) => {
        return {
            ...state,
            error: '',
        }
    }),
    on(invitationSuccess,(state,action) => {
        return {
            ...state,
            error: '',
            data: action.data,
        }   
    }),
    on(invitationFailure,(state,action) => {
        return {
            ...state,
            data: [],
            error: action.err,
        }
    })
    
)