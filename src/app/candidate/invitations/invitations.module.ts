import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvitationsComponent } from './invitations/invitations.component';
import { EachInvitationComponent } from './each-invitation/each-invitation.component';
import { InvitationsRoutingModule } from './invitations-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { InvitesComponent } from './invites.component';
import { Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { InviteEffect } from './store/invitations.effects';
import { invitationsReducer } from './store/invitations.reducers';



@NgModule({
  declarations: [
    InvitationsComponent,
    EachInvitationComponent,
    InvitesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    InvitationsRoutingModule,
    StoreModule.forFeature('invitations',invitationsReducer),
    EffectsModule.forFeature([InviteEffect])
  ],
  providers: []
})
export class InvitationsModule { }
