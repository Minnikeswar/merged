export interface EachInvitation{
    companyName: string;
    inviteId: string;
    inviter: string;
    jobDescription: string;
    jobMode: string;
    jobSalary: string;
    jobScope: string;
    jobTitle: string;
    jobType: string;
    salaryType: string;
    status?: number;
}

export const initialInvitation = {
    companyName: "",
    inviteId: "",
    inviter: "",
    jobDescription: "",
    jobMode: "",
    jobSalary: "",
    jobScope: "",
    jobTitle: "",
    jobType: "",
    salaryType: "",
    status: -1,
}