import { Component } from '@angular/core';
import { InviteService } from '../services/invite.service';
import { EachInvitation } from '../models/eachinvitation.model';
import { Store } from '@ngrx/store';
import { getInvitations } from '../store/invitations.actions';
import { allInvitations } from '../store/invitations.selectors';
import Swal from 'sweetalert2';


export interface Update{
  update: boolean;
}

@Component({
  selector: 'app-invitations',
  templateUrl: './invitations.component.html',
  styleUrls: ['./invitations.component.scss']
})
export class InvitationsComponent {

  invites: EachInvitation[] = [];
  invitationsLoading:boolean = true;
  value: number = -2;
  
  constructor(private inviteService: InviteService,private store: Store){

  }


  ngOnInit(){
      this.store.dispatch(getInvitations())
      this.store.select(allInvitations).subscribe((data: EachInvitation[]) => {
        this.invites = data
        this.invitationsLoading = false 
      })
  }
  
  

  

  acceptInvite(invitationId: string){
    this.inviteService.sendResponse(invitationId,1).subscribe((data: Update) => {
      if(data.update === true){
        const msg = 'You have successfully accepted the invitation'
          Swal.fire({
              title: 'Success',
              text: msg,
              icon: "success"
          });
          this.value = 1;
          this.invites = this.invites.map((eachInvite: EachInvitation) => {
            if(eachInvite.inviteId === invitationId){
              return {
                ...eachInvite,
                status: 1,
              }
            }
            return eachInvite;
          })

      }
    })
  }

  rejectInvite(invitationId: string){
    this.inviteService.sendResponse(invitationId,-1).subscribe((data: {update: boolean}) => {
      if(data.update === true){
        const msg = 'You have successfully rejected the invitation'
          Swal.fire({
              title: 'Success',
              text: msg,
              icon: "success"
          });
          this.value = -1;
          this.invites = this.invites.map((eachInvite: EachInvitation) => {
            if(eachInvite.inviteId === invitationId){
              return {
                ...eachInvite,
                status: -1,
              }
            }
            return eachInvite
          })
      }
    })
  }
}
