import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EachInvitationComponent } from './each-invitation.component';

describe('EachInvitationComponent', () => {
  let component: EachInvitationComponent;
  let fixture: ComponentFixture<EachInvitationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EachInvitationComponent]
    });
    fixture = TestBed.createComponent(EachInvitationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
