import { Component, EventEmitter, Input, Output } from '@angular/core';
import { InviteService } from '../services/invite.service';
import Swal from 'sweetalert2';
import { EachInvitation, initialInvitation } from '../models/eachinvitation.model';

@Component({
  selector: 'app-each-invitation',
  templateUrl: './each-invitation.component.html',
  styleUrls: ['./each-invitation.component.scss']
})
export class EachInvitationComponent {

  constructor(){}

  @Input() invitation: EachInvitation = initialInvitation; 

  @Output() acceptInvite: EventEmitter<string> = new EventEmitter<string>();

  @Output() rejectInvite: EventEmitter<string> = new EventEmitter<string>();

  value: number | undefined = -1;
  
  ngOnInit(){
    this.value = this.invitation.status
  }

  acceptInvitation(invitationId: string){
    this.acceptInvite.emit(invitationId)
  }

  rejectInvitation(invitationId: string){
    this.rejectInvite.emit(invitationId)
  }
}
