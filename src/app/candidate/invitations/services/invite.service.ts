import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment.development';
import { Update } from '../invitations/invitations.component';

@Injectable({
  providedIn: 'root'
})
export class InviteService {

  constructor(private http: HttpClient) { 
    
  }

  getInvitations(){
    const url = environment.invitationsUrl
    return this.http.get(url)
  }

  getInvitationsDetails(id: string){
    const url = `${environment.invitationsUrl}/${id}`
    return this.http.get(url)
  }

  sendResponse(id: string,val: number) : Observable<Update>{
    const url = `${environment.invitationsUrl}/${id}`
    return this.http.post(url,{val}).pipe(
      map((resp) => resp as Update)
    )
  }
}

