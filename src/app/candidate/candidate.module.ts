import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CandidateRoutingModule } from './candidate-routing.module';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { CandidateComponent } from './candidate.component';
import { TrimspacesDirective } from './directives/trimspaces.directive';


@NgModule({
  declarations: [
    CandidateComponent,
  ],
  imports: [
    CommonModule,
    CandidateRoutingModule,
    RouterModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
  ]
})
export class CandidateModule { }
