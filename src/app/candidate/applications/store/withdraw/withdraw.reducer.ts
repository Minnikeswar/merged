import { createReducer, on } from "@ngrx/store";
import { CombinedJobs } from "../../models/MyJobsApplications.model";
import { setApplications, withDrawFailure, withDrawSuccess } from "./withdraw.action";

export interface ApplicationsState{
    data: CombinedJobs[],
    error: string,
}


export const initialApplicationsState: ApplicationsState = {
    data: [],
    error: '',
}  


export const withDrawReducer = createReducer(
    initialApplicationsState,
    on(setApplications,(state,action) => {
        return {
            data: action.data,
            error: '',
        }
    }),
    on(withDrawSuccess,(state,action) => {
        const withDrawId = action.applicationId
        console.log('in reducer',withDrawId)
            return {
                ...state,
                data: state.data.filter((each: any) => each.applicationId !== withDrawId)
            }
    }),
    on(withDrawFailure,(state,action) => {
        return {
            ...state,
            error: action.error
        }
    })
)