import { ApplicationsState } from './withdraw.reducer';
import { createFeatureSelector, createSelector } from "@ngrx/store";


export const withDrawFeatureSelector = createFeatureSelector<ApplicationsState>('withdraw')

export const withDrawData = createSelector(withDrawFeatureSelector,state => state.data)