import { createAction, props } from "@ngrx/store";
import { CombinedJobs } from "../../models/MyJobsApplications.model";

export const withDrawRequest = createAction('[withDraw] request',props<{applicationId: string}>());

export const withDrawSuccess = createAction('[withDraw] success',props<{applicationId: string}>())

export const withDrawFailure = createAction('[withDraw] failure',props<{error: string}>())

export const setApplications = createAction('[applied jobs]',props<{data: CombinedJobs[]}>())