import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as WithDrawActions from './withdraw.action'
import { environment } from "src/environments/environment.development";
import { catchError, exhaustMap, map, of } from "rxjs";


// this.actions$.pipe(
//     ofType(ApplyActions.withDrawRequest),
//     exhaustMap((action) => {
//         let withDrawUrl = `${environment.withDrawUrl}/${action.applicationId}`;
//         return this.http.delete(withDrawUrl).pipe(map((res: any) => {
//             return res
//         }),
//         catchError((err: any) => {
//             return of(ApplyActions.withDrawFailure({error: err}))
//         }))
//     })
// )


@Injectable()
export class WithDrawEffect{

    actions$ = inject(Actions)
    constructor(private http: HttpClient,private router: Router){}

    withDrawEffect$ = createEffect(() => 
        this.actions$.pipe(
            ofType(WithDrawActions.withDrawRequest),
            exhaustMap((action) => {
                let withDrawUrl = `${environment.withDrawUrl}/${action.applicationId}`;
                return this.http.delete(withDrawUrl).pipe(map((res: any) => {
                    return WithDrawActions.withDrawSuccess({applicationId: action.applicationId})
                    // return res
                }),
                catchError((err: any) => {
                    return of(WithDrawActions.withDrawFailure({error: err}))
                })
                )
            })
        )    
    )

}