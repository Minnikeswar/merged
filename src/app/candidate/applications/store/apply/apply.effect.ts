import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as ApplyActions from '../apply/apply.action'
import { catchError, exhaustMap, map, of, switchMap } from "rxjs";
import Swal from "sweetalert2";
import { GetTokenService } from "src/app/shared/services/get-token.service";
import { ApplicationData } from "./apply.model";
import { environment } from 'src/environments/environment.development';
import { WithDrawResponse } from '../../models/withDraw.model';


interface AlreadyApplied{
    applied: boolean;
}

@Injectable()
export class ApplyEffect{
    actions$ = inject(Actions)
    constructor(private http: HttpClient,private tokenService: GetTokenService,private route: Router){
        
    }
    
    applyRequest$ = createEffect(() => 
         this.actions$.pipe(
            ofType(ApplyActions.applyRequest),
            switchMap((action) => {
                let url = 'http://localhost:7000/apply/'
                // let token = localStorage.getItem('cur_token')
                // token = JSON.parse(token)
                const token = this.tokenService.jwtToken()
                const headers = new HttpHeaders({
                'authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
                }) 
                return this.http.get(url+action.jobId,{headers}).pipe(
                    map((data:any) => {
                        if(data.applied===true){
                            Swal.fire({
                                icon: "error",
                                title: "Oops...",
                                text: "You have already applied for this Job",
                              });
                              this.route.navigateByUrl('/cand/jobs/all')
                            // this.messageService.add({severity: 'error', summary: 'Already Applied',detail: 'You have already applied for this job'})
                            return ApplyActions.applyFailure({error: 'Already Applied'})
                        }
                        return ApplyActions.applySuccess({data: data})
                    })
                )
                
            })
        )
    )

  



}   
