import { createAction, props } from "@ngrx/store";
import { ApplicationData } from "./apply.model";

export const applyRequest = createAction('[apply] Request',props<{jobId: string}>());

export const applySuccess = createAction('[apply] Success',props<{data: ApplicationData}>());

export const applyFailure = createAction('[apply] Failure',props<{error: string}>());

export const onLogout = createAction('[logout] action');

