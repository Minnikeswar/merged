import {  createFeatureSelector, createReducer, createSelector, on } from "@ngrx/store";
import * as ApplyActions from '../apply/apply.action'
import { ApplicationData, initialProfile } from "./apply.model";

export interface ApplyState{
    data: ApplicationData;
    message: string;
    error: string;
}

export const applyInitialState: ApplyState = {
    data: {
        companyLogo: '',
        companyName: '',
        jobTitle: '',
        location: '',
        prevData:  initialProfile,
    },
    message: '',
    error: '', 
}

export const applyReducer = createReducer(
    applyInitialState,
    on(ApplyActions.applyRequest,(state) => {
        return {
            ...state,
            error: '',
        }
    }),
    on(ApplyActions.applySuccess,(state,{data}) => {
        return {
            ...state,
            data: data,
            error: '',
        }
    }),

    on(ApplyActions.onLogout,(state: ApplyState) => {
        return {
            ...state,
            data: {
                companyLogo: '',
                companyName: '',
                jobTitle: '',
                location: '',
                prevData:  initialProfile,
            },
            error: '',
            message: '',
        }
    }) 
    // on(ApplyActions.applyFailure,(state,{error}) => {
    //     return {
    //         ...state,
    //         error: error,
    //     }
    // })
)


const selectApplyState = createFeatureSelector<ApplyState>('apply')
export const apply = createSelector(selectApplyState,(state: ApplyState) => state.data)

