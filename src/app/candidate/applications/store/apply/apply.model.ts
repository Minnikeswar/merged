export interface ApplicationData{
    companyLogo: string;
    companyName: string;
    jobTitle: string;
    location: string;
    prevData: Profile;
}


interface Profile{
    dob: string;
    email: string;
    firstName: string;
    github: string;
    lastName: string;
    linkedin: string;
    location: string;
    org: string;
    phone: string;
    profileUrl: string;
    userAbout: string;
    userBio: string;
    userSkills: string[];
    username: string;
    __v: number;
    _id: string;
}

export const initialProfile = {
    dob: '',
    email: '',
    firstName: '',
    github: '',
    lastName: '',
    linkedin: '',
    location: '',
    org: '',
    phone: '',
    profileUrl: '',
    userAbout: '',
    userBio: '',
    userSkills: [],
    username: '',
    __v: 0,
    _id: '',
}