import { Component, Input } from '@angular/core';
import { FetchService } from '../services/fetch.service';
import { Subscription, map } from 'rxjs';
import Swal from 'sweetalert2';
import { Store } from '@ngrx/store';
import { isMainLoading } from '../store/mainloading/mainload.selector'
import { Router } from '@angular/router';
import { MyJobsApplications } from '../models/MyJobsApplications.model';
import { WithDrawResponse } from '../models/withDraw.model';

@Component({
  selector: 'app-applied',
  templateUrl: './applied.component.html',
  styleUrls: ['./applied.component.scss']
})

export class AppliedComponent {
   isMainLoading: boolean | null = true
   loadingSubcription: Subscription | null = null;
   applySubscription: Subscription | null = null;

   @Input('jobs') jobStreetJobs: MyJobsApplications[] = [];

  constructor(private fetch: FetchService, private store: Store,private router: Router){}
    ngOnInit(){
      
      this.loadingSubcription = this.store.select(isMainLoading).subscribe((data:boolean) => {
          this.isMainLoading = data
      })
    }


    withDraw(applicationId: string){
      Swal.fire({  
        title: 'Withdraw Application',  
        text: 'Are you sure you want to withdraw the application for this Job Post?',  
        icon: 'warning',  
        showCancelButton: true,  
        confirmButtonText: 'Yes, withdraw',  
        cancelButtonText: 'Cancel'  
      }).then((result) => { 
        if (result.value) { 
          this.fetch.withDraw(applicationId).pipe(map((res) => res)).subscribe((res:WithDrawResponse) => {
            if(res["message"]){
              this.jobStreetJobs = this.jobStreetJobs.filter((job:MyJobsApplications) => {
                return job.applicationId !== applicationId
              })
              Swal.fire(  
                'Deleted!',  
                'Your withdrawn has been successful.',  
                'success'  
              )
            }
          }) 
          this.router.navigate(['/cand/applications/all'])
        } else if (result.dismiss === Swal.DismissReason.cancel) {  
          Swal.fire(  
            'Cancelled',  
            'Your Job Post is Safe :)',  
            'error'  
          )  
        }  
      })  
      
    }

    ngOnDestroy(){
      if(this.loadingSubcription){
        this.loadingSubcription.unsubscribe()
      }
      if(this.applySubscription){
        this.applySubscription.unsubscribe()
      }
    }
}
