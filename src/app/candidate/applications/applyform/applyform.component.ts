import { ApplyState, apply } from '../store/apply/apply.reducer';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as ApplyActions from '../store/apply/apply.action'
import { Subscription, catchError, map, of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FetchService } from '../services/fetch.service';
import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';
import { ApplyForm } from '../models/applyForm.model';
import { ApplicationData } from '../store/apply/apply.model';



export interface ParamId{
  id: string;
}

@Component({
  selector: 'app-applyform',
  templateUrl: './applyform.component.html',
  styleUrls: ['./applyform.component.scss']
})
export class ApplyformComponent {

  isLoading: boolean = true;
  routerSubscription: Subscription | null = null;
  applySubscription: Subscription | null = null;
  jobId: string = "";
  companyName: string = "";
  companyLogo:string = "";
  location: string = "";
  jobTitle:string = "";

  applyForm: ApplyForm = {
    firstname: '',
    lastname: '',
    email: '',
    phone: '',
    bio: '',
    position: '',
    startdate: new Date(),
    resume: null,
  }

  constructor(private store: Store<{applyInitialState : ApplyState}>,private router: ActivatedRoute,
    private fetch: FetchService,private messageService: MessageService,private route: Router) {

  }
  
  
  ngOnInit(){
    
    this.applySubscription = this.store.select(apply).subscribe((data:ApplicationData) => {

      if(data.prevData==null) return

      const res = data.prevData  
      this.companyLogo = data.companyLogo
      this.companyName = data.companyName
      this.location = data.location
      this.jobTitle = data.jobTitle
      this.applyForm = {
        firstname: res.firstName,
        lastname: res.lastName,
        email: res.email,
        phone: res.phone,
        bio: res.userBio,
        position: data.jobTitle,
        startdate: new Date(),
        resume: null,
      }
      setTimeout(() => {
        this.isLoading = false
      },500)
    })


    this.routerSubscription = this.router.queryParams.subscribe((params) => {
      this.jobId = params['id']
      this.store.dispatch(ApplyActions.applyRequest({jobId: params['id']}))
    })
  }


  onFileChange(event: Event){
    const input = event.target as HTMLInputElement
    if (input.files && input.files.length > 0) {
      this.applyForm.resume = input.files[0];
    }
    
  }

  submitForm(){
    this.fetch.submitForm(this.applyForm,this.jobId).pipe(map((res) => {
      
      return res;
      
    }),catchError((err) => {
      this.messageService.add({severity: 'error', summary: 'Unknown error occured',detail: 'Application not submitted'})
      return of([])
    })).subscribe((res:any) => {
      if(res.errors?.startDate.value==='undefined'){
        this.messageService.add({severity: 'error', summary: 'Incomplete Details',detail: 'Start Date is Required'})
        return;
      }
      Swal.fire({
        position: "center",
        icon: "success",
        title: "Application Submitted Successfully",
        showConfirmButton: false,
        timer: 1500
      });
      setTimeout(() => {
        this.route.navigateByUrl('/cand/applications/all')
      },1000)
    })
    
  }

  ngOnDestroy(){
    if(this.routerSubscription){
      this.routerSubscription.unsubscribe()
    }
    if(this.applySubscription){
      this.applySubscription.unsubscribe()
    }
  }
}
