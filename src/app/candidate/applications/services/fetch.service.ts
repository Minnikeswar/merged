import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { mainLoadAction } from '../store/mainloading/mainload.actions';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.development';
import { Observable, Subject, map } from 'rxjs';
import { AllApplications, Applications } from '../models/applications.model';
import { CombinedJobs, MyJobsApplications } from '../models/MyJobsApplications.model';
import { WithDrawResponse } from '../models/withDraw.model';
import { withDrawRequest } from '../store/withdraw/withdraw.action';

@Injectable({
  providedIn: 'root'
})
export class FetchService {

  constructor(private store: Store,private http: HttpClient) { }

  getAppliedJobs(): Observable<AllApplications>{
    this.store.dispatch(mainLoadAction({status: true}))
    const fetchUrl = environment.appliedUrl
    return this.http.get(fetchUrl).pipe(map((res: any) => res as AllApplications))
}

 getApplied(){
    this.getAppliedJobs()
 }

  withDraw(applicationId: string): Observable<WithDrawResponse>{

    this.store.dispatch(withDrawRequest({applicationId: applicationId}))

    const withDrawUrl = `${environment.withDrawUrl}/${applicationId}`
    return this.http.delete(withDrawUrl).pipe(map((res: any) => {
      return res
    }),map((res: any) => res as WithDrawResponse))
    
  }


  submitForm(formDetails: any,jobId: string){
    const applicationDetails = {
      ...formDetails,
      startDate: formDetails.startdate,
    }
    const applyUrl =  `http://localhost:7000/apply/${jobId}`         
    const formData = new FormData()
    formData.append('resume',formDetails.resume)
    formData.append('startDate',applicationDetails.startDate)
    return this.http.post(applyUrl,formData)
  }
}
