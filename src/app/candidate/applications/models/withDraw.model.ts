export interface WithDrawResponse{
    deletedDocument: DeletedDocument;
    message: string;
}

export interface DeletedDocument{
    acknowledged: boolean;
    deletedCount: number;
}