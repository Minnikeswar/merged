interface FileObject {
    name: string;
    lastModified: number;
    lastModifiedDate?: Date;
    webkitRelativePath: string;
    size: number;
  }

export interface ApplyForm{
    firstname: string;
    lastname: string;
    email: string;
    phone: string;
    bio: string;
    position: string;
    startdate: Date;
    resume: any;
      
}