import { FJob, initialStateFJob } from "../../jobs/models/FJob.model";
import { MyJobsApplications } from "./MyJobsApplications.model";

export interface Applications{
    applicationId: string;
    appliedAt: string;
    jobData: FJob;
    postedBy: string;
    startDate: string;
    status: number;
}

export const initalStateApplications: Applications = {
    applicationId: '',
    appliedAt: '',
    jobData: initialStateFJob,
    postedBy: '',
    startDate: '',
    status: 0,
}

export interface AllApplications{
   data: Applications[],
   formData: MyJobsApplications[]
}