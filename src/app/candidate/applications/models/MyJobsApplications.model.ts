import { Applications } from './applications.model';
import { Job } from "../../jobs/models/job.model";

export interface MyJobsApplications{
    applicationId: string;
    appliedAt: string;
    jobData: Job;
    startDate: string;
}

export type CombinedJobs = Applications & MyJobsApplications