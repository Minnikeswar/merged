import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationsRoutingModule } from './applications-routing.module';
import { ApplicationsComponent } from './applications.component';
import { AppliedComponent } from './applied/applied.component';
import { AllComponent } from './all/all.component';
import { FreelanceComponent } from './freelance/freelance.component';
import { EachAppliedComponent } from './each-applied/each-applied.component';
import { SharedModule } from 'primeng/api';
import { CandidateModule } from '../candidate.module';
import { Store, StoreModule } from '@ngrx/store';
import { mainLoadReducer } from './store/mainloading/mainload.reducer';
import { ApplyformComponent } from './applyform/applyform.component';
import { applyReducer } from './store/apply/apply.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ApplyEffect } from './store/apply/apply.effect';
import { withDrawReducer } from './store/withdraw/withdraw.reducer';
import { WithDrawEffect } from './store/withdraw/withdraw.effects';


@NgModule({
  declarations: [
    ApplicationsComponent,
    AppliedComponent,
    AllComponent,
    FreelanceComponent,
    EachAppliedComponent,
    ApplyformComponent
  ],
  imports: [
    CommonModule,
    ApplicationsRoutingModule,
    CandidateModule,
    SharedModule,
    StoreModule.forFeature('apply',applyReducer),
    StoreModule.forFeature('mainload',mainLoadReducer),
    StoreModule.forFeature('withdraw',withDrawReducer),
    EffectsModule.forFeature([ApplyEffect,WithDrawEffect])
  ]
})
export class ApplicationsModule { }
