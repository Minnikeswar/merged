
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { FetchService } from '../services/fetch.service';
import { Subscription, catchError, map, of } from 'rxjs';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { isMainLoading } from '../store/mainloading/mainload.selector';
import { mainLoadAction } from '../store/mainloading/mainload.actions';
import { AllApplications, Applications } from '../models/applications.model';
import { CombinedJobs, MyJobsApplications } from '../models/MyJobsApplications.model';
import { setApplications } from '../store/withdraw/withdraw.action';
import { withDrawData } from '../store/withdraw/withdraw.selector';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss']
})
export class AllComponent {

  checkedPage : string = 'tab1';
  allJobs: CombinedJobs[] = []
  isLoading: boolean = true;
  loadingSubscription: Subscription | null = null;
  applySubscription: Subscription | null = null;
  applicationsSubscription: Subscription | null = null;
  myJobs: MyJobsApplications[]= [];
  freelanceJobs: Applications[] = [];

  constructor(private fetch: FetchService,private store: Store,
    private messageService: MessageService,
    private router: Router
    ){}

  toggle(tab : string){
    this.checkedPage = tab;
  }

  ngOnInit(){
    
    this.loadingSubscription = this.store.select(isMainLoading).subscribe((data) => {
      this.isLoading = data
    })

    this.applicationsSubscription = this.store.select(withDrawData).subscribe((data: CombinedJobs[]) => {
      this.allJobs = data 
    })

    this.fetch.getAppliedJobs().pipe(map((data: AllApplications) => {

      this.store.dispatch(mainLoadAction({status: false}))
      let list = []
      let free = Object.values(data.data)
      let myjobs = Object.values(data.formData)
      for(let job of free){
        list.push(job)
        this.freelanceJobs.push(<Applications>job)
      }
      for(let job of myjobs){
        list.push(job)
        this.myJobs.push(<MyJobsApplications>job)
      }     
      return list as CombinedJobs[]
    }),catchError((err: any) => {
      this.store.dispatch(mainLoadAction({status: false}))
      if(err.error.error === 'jwt expired' || err.error.error === 'jwt malformed'){
        this.messageService.add({severity: 'error', summary: 'Session Expired', detail: 'Unfortunately, the session has expired :-(' })
        localStorage.removeItem('cur_token')
        setTimeout(() => {
          this.router.navigateByUrl('/login')
        },1000)
      }
      return of([])
    })).subscribe((res:CombinedJobs[]) => {
      this.allJobs = res 
      this.store.dispatch(setApplications({data: this.allJobs}))
    })
  }

  ngOnDestroy(){
    if(this.loadingSubscription){
      this.loadingSubscription.unsubscribe()
    }
    if(this.applicationsSubscription){
      this.applicationsSubscription.unsubscribe()
    }
  }
}
