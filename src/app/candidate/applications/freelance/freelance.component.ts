
import {  map } from 'rxjs';
import { FetchService } from '../services/fetch.service';
import { Component, Input } from '@angular/core';
import Swal from 'sweetalert2';
import { Applications } from '../models/applications.model';
import { WithDrawResponse } from '../models/withDraw.model';

@Component({
  selector: 'app-freelance',
  templateUrl: './freelance.component.html',
  styleUrls: ['./freelance.component.scss']
})
export class FreelanceComponent {
  isFreelanceLoading: boolean = false;

  @Input('jobs') freelanceJobs: Applications[] = [];
  

  constructor(private fetch: FetchService){}

  ngOnInit(){ 

  }


  withDraw(applicationId: string){
    Swal.fire({  
      title: 'Withdraw Application',  
      text: 'Are you sure you want to withdraw the application for this Job Post?',  
      icon: 'warning',  
      showCancelButton: true,  
      confirmButtonText: 'Yes, withdraw',  
      cancelButtonText: 'Cancel'  
    }).then((result) => { 
      if (result.value) { 
        this.fetch.withDraw(applicationId).pipe(map((res) => res)).subscribe((res:WithDrawResponse) => {
          if(res["message"]){
            this.freelanceJobs = this.freelanceJobs.filter((job:Applications) => {
              return job.applicationId !== applicationId
            })
            Swal.fire(  
              'Deleted!',  
              'Your withdrawn has been successful.',  
              'success'  
            )

          }
        }) 
          
      } else if (result.dismiss === Swal.DismissReason.cancel) {  
        Swal.fire(  
          'Cancelled',  
          'Your Job Post is Safe :)',  
          'error'  
        )  
      }  
    })  
  }
}
