import { initalStateApplications } from './../models/applications.model';
import { Applications } from '../models/applications.model';
import { MyJobsApplications } from './../models/MyJobsApplications.model';

import { Component, EventEmitter, Input, Output } from '@angular/core';

interface ApplicationStatus{
  1: 'Pending';
  2: 'Accepted';
  0: 'Rejected';
}



@Component({
  selector: 'app-each-applied',
  templateUrl: './each-applied.component.html',
  styleUrls: ['./each-applied.component.scss']
})
export class EachAppliedComponent {

  APPLICATION_STATUS: any = {
    1: 'Pending',
    2: 'Accepted',
    0: 'Rejected',
  }

  @Output() delete : EventEmitter<string> = new EventEmitter();
  
  @Input('eachJob') appliedJobs: any = initalStateApplications;

  @Input('isFreelance') isFreelance: boolean = false;

  @Input('isShowDelete') showDelete: boolean = false;

  applicationStatus: string | null = null;
  
  constructor(){

  }

  ngOnInit(){
    if(this.isFreelance){
      this.applicationStatus = this.APPLICATION_STATUS[this.appliedJobs.status];

    }
  }

  withDraw(){
      this.delete.emit(this.appliedJobs.applicationId)
  }  

  getStyle(applicationStatus: string){
    return {
      'background-color': this.applicationStatus === 'Pending' ? 'gold' : this.applicationStatus === 'Rejected' ? 'red': 'green',
      'color': 'white',
      'padding': '13px',
      'opacity': 0.9,
      'border-radius': '14px'   
    };
  }

}
