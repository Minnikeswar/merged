import { Directive, ElementRef } from '@angular/core';
import { Subscription, fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrimspaces]'
})
export class TrimspacesDirective {

  textSubscription: Subscription | null = null;

  constructor(private element: ElementRef) {
    this.textSubscription = fromEvent(this.element.nativeElement,'input').subscribe((event: any) => {
      if(event.target.value===null)return;
      event.target.value = event.target.value.trim()
    })
   }
   
   ngOnDestroy(){
    if(this.textSubscription){
      this.textSubscription.unsubscribe()
    }
   }

}
