import { loading } from '../store/loginLoading/loading.selector';
import {  loadingAction } from '../store/loginLoading/loading.action';
import { Component } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Store } from '@ngrx/store';
import * as AuthActions from '../store/auth/auth.action'
import {MessageService} from 'primeng/api'
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs';
import { Router } from '@angular/router';
import { checkValidaion, strongPasswordValidator } from '../validators/password.validation';


interface LoginDetails{
  username: string;
  password: string;
}

export interface SignUpDetails{
  username: string;
  email: string;
  password: string;
  confirm: string;

}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent {
  loginLeft: string = '50px';
  signupLeft: string = '450px';
  buttonLeft: string = '0px';
  username: string = "";
  password: string = "";
  signUpForm: FormGroup = new FormGroup({});
  submitted: boolean = false;
  isShow: boolean = false;
  isLoading: boolean | null = null;

  toggleShow(){
    this.isShow =!this.isShow;
  }

  constructor(private store: Store,private loginService: LoginService ,private route: Router, private messageService : MessageService){
    this.signUpForm = new FormGroup({
      username: new FormControl('',[Validators.required]),
      email: new FormControl('',[Validators.required,Validators.email]),
      password: new FormControl('',[Validators.required,strongPasswordValidator()]),
      acceptTerms: new FormControl(false,[Validators.requiredTrue]),
      confirm: new FormControl('',[Validators.required])

    });
  }

  ngOnInit(){
    this.store.select(loading).subscribe((res:boolean) => {
      this.isLoading = res 
    })
  }
  signup(): void {
    this.loginLeft = '-400px';  
    this.signupLeft = '50px';   
    this.buttonLeft = '110px';  
  }

  login(): void {
    this.loginLeft = '50px';   
    this.signupLeft = '450px'; 
    this.buttonLeft = '0px'; 
  }

  loginObj:LoginDetails = {
    username: this.username,
    password: this.password,
  }
  onLogin(){
    this.isShow = false
    this.store.dispatch(loadingAction({status: true}))
    this.store.dispatch(AuthActions.loginRequest({username: this.username,password: this.password}))
    this.username = ''
    this.password = ''
  }

  onKeyDown(event: KeyboardEvent){
    this.isShow = false
    if(event.key === 'Enter'){
      this.onLogin()
    }
  }

  isValidPassword(password: string){
     return checkValidaion(password)
  }

  get f(){
    return this.signUpForm.controls
  }

  get p(){
    return this.signUpForm.get('password')
  }
  onSignUp(){
    this.submitted = true;
    if(this.signUpForm.invalid){
      return;
    }
    const signUpDetails: SignUpDetails = {
      username: this.signUpForm.controls['username'].value,
      email: this.signUpForm.controls['email'].value,
      password: this.signUpForm.controls['password'].value,
      confirm: this.signUpForm.controls['confirm'].value,
    }
    
    this.store.dispatch(loadingAction({status: true}))
    this.loginService.signUp(signUpDetails).pipe(map((res:any) => res)).subscribe((res) => {
      setTimeout(() => {
        this.store.dispatch(loadingAction({status: false}))
      },500)
      if(res['alreadyExists'] === true){
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'User already exists. Try Again' });
        return;
      }
      if(res['mailExists']===true){
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Email already exists. Try again' });
        return;
      }
      this.messageService.add({ severity:'success', summary: 'Success', detail: 'Signup Successful' });
      this.route.navigateByUrl('/cand/auth/login')
      return;
    })
    this.onReset()
  }

  onReset(){
    this.signUpForm.reset()
    this.submitted = false;
  }
}
