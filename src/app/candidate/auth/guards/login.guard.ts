import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

export const loginGuard: CanActivateFn = (route, state) => {
  const router = inject(Router)
  const token = localStorage.getItem('cur_token')
  if(token){
    router.navigateByUrl(state.url)
    return false;
  }
  return true;
};
