import { createAction, props } from "@ngrx/store";
import { State } from "./auth.reducer";

export const loginRequest = createAction('login Request', props<{username: string,password: string}>())
export const loginSuccess = createAction('login Success', props<{data: State}>())

export const loginFailure = createAction('login Failure', props<{error: string}>());
