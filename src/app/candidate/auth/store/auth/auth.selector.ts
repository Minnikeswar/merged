import { State } from './auth.reducer';
import {  createFeatureSelector, createSelector } from "@ngrx/store";


const loginLoaderState = createFeatureSelector<State>('auth')


export const authUsername = createSelector(loginLoaderState,(state: State) => {
    return state.username
})

