import { Store } from '@ngrx/store';
import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as AuthActions from '../auth/auth.action'
import { catchError, map, of, switchMap, tap } from "rxjs";
import {  Router } from "@angular/router";
import { MessageService } from 'primeng/api';
import { loadingAction } from '../loginLoading/loading.action';
import { environment } from 'src/environments/environment.development';

@Injectable()
export class AuthEffect{
    actions$ = inject(Actions)
    url = environment.loginUrl
    constructor(private http: HttpClient,private route: Router,
        private store: Store,
        private messageService: MessageService){}
    loginRequest$ = createEffect(() => 
        this.actions$.pipe(
        ofType(AuthActions.loginRequest),
            switchMap((action) => {
                const userDetails = {
                    username: action.username,
                    password: action.password
                }
                return this.http.post(this.url,userDetails).pipe(
                    map((data: any) => {
                        this.store.dispatch(loadingAction({status: false}))
                        if(data.userExists === false){
                            this.messageService.add({ severity: 'error', summary: 'Login Failed', detail: 'Invalid Credentials' });
                            return AuthActions.loginFailure({error: data.message})
                        }
                        if(data.password === false){
                            this.messageService.add({ severity: 'error', summary: 'Try Again', detail: 'Incorrect Password' });
                            return AuthActions.loginFailure({error: 'Incorrect Password'})
                            
                        }
                        const res = {
                            username: action.username,
                            message: data.message,
                            token: data.token,
                            error: data.error,
                        }
                        
                        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Login Successful' });
                        return AuthActions.loginSuccess({data: res})
                    }),
                    catchError(error => {
                        this.store.dispatch(loadingAction({status: false}))
                        this.messageService.add({ severity: 'error', summary: 'Login Failed', detail: 'Unknown Error' });
                        return of(AuthActions.loginFailure({error: error}))
                        }
                    )
                )
            })
        )
    )

    loginSuccess$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(AuthActions.loginSuccess),
            tap(({data}) => {
                localStorage.setItem('cur_token',JSON.stringify(data.token))
                localStorage.setItem('cur_username',JSON.stringify(data.username))
                // alert(`Login successful, Welcome ${data.username}`)
                setTimeout(() => {
                    this.route.navigate([`/cand/jobs/all`])
                },2000)
            })
        )
    },{dispatch: false})

    

}