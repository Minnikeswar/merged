import { createFeatureSelector, createReducer, createSelector, on } from "@ngrx/store";
import { loginFailure, loginRequest, loginSuccess } from "./auth.action";

 export interface State{
    username: string;
    message: string;
    error: string;
    token: string;
 } 

 export const initialState: State = {
    username: '',
    message: '',
    error: '',
    token: ''
 }

 export const authReducer = createReducer(initialState,
        on(loginRequest,(state,action) => {
            return {
                ...state,
                username: action.username,
                error: ''
            }
        }),
        on(loginSuccess,(state,{data}) => {
            return {
                ...state,
                 username: data.username,
                 message: data.message,
                 error: '',
                 token: data.token,
            }
        }),
        on(loginFailure,(state,{error}) => {
            return {
                ...state,
                token: '',
                error: error
            }
        })
    )

const selectAuthState = createFeatureSelector<State>('auth')
export const selectUsername = createSelector(selectAuthState,state => state.username)