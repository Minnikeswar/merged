import { Component, Input } from '@angular/core';
import { SimilarJob, initialStateSimilar } from '../models/similarJobs.model';

@Component({
  selector: 'app-similar',
  templateUrl: './similar.component.html',
  styleUrls: ['./similar.component.scss']
})
export class SimilarComponent {
  @Input() eachElement: SimilarJob = initialStateSimilar;

  ngOnInit(){
    
  }

}
