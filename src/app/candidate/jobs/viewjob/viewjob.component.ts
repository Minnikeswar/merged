import { FetchService } from './../services/fetch.service';
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, map } from 'rxjs';
import { ViewJob } from '../models/viewJob.model';

@Component({
  selector: 'app-viewjob',
  templateUrl: './viewjob.component.html',
  styleUrls: ['./viewjob.component.scss']
})
export class ViewjobComponent {

  @Input() jobCard: ViewJob | null = null;

  isViewLoading: boolean = true;
 
  routeSubscription: Subscription | null = null;

  constructor(private activatedRoute: ActivatedRoute,private fetch: FetchService){}

  ngOnInit(){
    this.routeSubscription = this.activatedRoute.queryParams.subscribe(param => {

        this.fetch.getViewJob(param['id']).subscribe(data => {
          const jobDetails = data['job_details'];
          const temp = {
            title: jobDetails.title,
            description: jobDetails.job_description,
            rating: jobDetails.rating,
            location: jobDetails.location,
            employType: jobDetails.employment_type,
            companyLogo: jobDetails.company_logo_url,
            package: jobDetails.package_per_annum,
            lifeAt: jobDetails.life_at_company.description,
            imageLifeAt: jobDetails.life_at_company.image_url,
            skills: jobDetails.skills,
            similarJobs: data['similar_jobs']

          }
          setTimeout(() => {
            this.jobCard = temp
            this.isViewLoading = false;
          },2000)
          
        })
    })
  }

  ngOnDestroy(){
    if(this.routeSubscription){
      this.routeSubscription.unsubscribe()
    }
  }

}
