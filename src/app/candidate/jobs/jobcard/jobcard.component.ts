import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Job, initialStateJob } from '../models/job.model';
import { FetchService } from '../services/fetch.service';

@Component({
  selector: 'app-jobcard',
  templateUrl: './jobcard.component.html',
  styleUrls: ['./jobcard.component.scss']
})
export class JobcardComponent{


  constructor(private fetch: FetchService){
  }

  each: Job = initialStateJob;
  
  @Input() job:Job = initialStateJob;

  @Input() isLiked!:boolean;
 

  @Input() showLike:boolean | null = null; 

  public isButtonHeartOn = false;
  public size: number = 70;

  onClickButtonHeart(){
    this.fetch.onHeartClick(this.each.id,this.isLiked)
    this.isLiked = !this.isLiked;
  }

  ngOnInit(){ 
    this.each = this.job;
  }
}
