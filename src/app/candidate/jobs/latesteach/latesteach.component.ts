import { Component, Input } from '@angular/core';
import { FreelanceJob, initialStateFreelanceJob } from '../models/freelanceJob.model';
import { MdbModalRef, MdbModalService } from 'mdb-angular-ui-kit/modal';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { FJob, initialStateFJob } from '../models/FJob.model';

@Component({
  selector: 'app-latesteach',
  templateUrl: './latesteach.component.html',
  styleUrls: ['./latesteach.component.scss']
})
export class LatesteachComponent {

  @Input() job : FreelanceJob = initialStateFreelanceJob;

  eachJob: FJob = initialStateFJob;

  modalRef: MdbModalRef<ModalComponent> | null = null;

  constructor(private modalService: MdbModalService){
  }
  ngOnInit(){
    this.eachJob = this.job.job;
  }

  openModal() {
    this.modalRef = this.modalService.open(ModalComponent,{
      data: {
        jobTitle: this.eachJob.jobTitle,
        jobDescription: this.eachJob.jobDescription,
        jobExperience: this.eachJob.jobExperience,
        jobScope: this.eachJob.jobScope,
        jobSkills: this.eachJob.jobSkills,
        salaryType: this.eachJob.salaryType,
        jobSalary: this.eachJob.jobSalary,
        jobMode: this.eachJob.jobMode,
        jobType: this.eachJob.jobType,
        companyName: this.eachJob.companyName,
        postedBy: this.job.recruiter,
        datePosted: this.eachJob.datePosted,
      }
    })
  }

}
