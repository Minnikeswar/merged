import { Injectable } from '@angular/core';
import {data} from '../../../../../data'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.development';
import { Observable, Subject, map } from 'rxjs';
import { LikesData } from '../models/myjobs.model';
import { Like } from '../models/myjobs.model';
import { FreelanceJob } from '../models/freelanceJob.model';

@Injectable({
  providedIn: 'root'
})

export class FetchService {

  constructor(private http: HttpClient) { }

  customSubject = new Subject<string[]>();

  getLatestJobs(): Observable<FreelanceJob[]>{
    const latestUrl = environment.latestJobsUrl
    return this.http.get(latestUrl).pipe(map((res: any) => <FreelanceJob[]>res))
  }


  getViewJob(jobId: string): Observable<any>{
    const viewUrl = `${environment.viewJobUrl}/${jobId}`
    const mainToken =  environment.mainToken

    // SHOULD NOT MODIFY THIS

    const headers = new HttpHeaders({
      'Authorization':  `Bearer ${mainToken}`
    })
    return this.http.get(viewUrl,{headers})
    
  }

  onHeartClick(jobId: string,isRed: boolean){
    const url = `${environment.likeUrl}/${jobId}`
    let updatedData = []
    if(isRed){
      this.http.delete(url).pipe(map((res: any) => <LikesData>res),map((data: LikesData) => {
        console.log('fetch',data)
        return data['likes'] as Like[]
      })).subscribe((data: Like[]) => {
        updatedData = data.map(each => each['like_id'])
        this.customSubject.next(updatedData)
      })
    }
    else{
      this.http.get(url).pipe(map((res: any) => <LikesData>res),map((data:any) => data['likes'])).subscribe(data => {
        updatedData = data.map((each:any) => each['like_id'])
        this.customSubject.next(updatedData)
      })
    }
  }

  getUpdatedData(): Observable<string[]>{
    return this.customSubject.asObservable()
  } 

  getSaved(): Observable<LikesData>{
    const url = environment.getSavedUrl
    return this.http.get(url).pipe(map((res: any) => res as LikesData))
  }

  getJobsData(){
    return data;
  }
}
