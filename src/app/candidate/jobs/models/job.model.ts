export interface Job{
    companyLogo: string,
    companyName: string,
    datePosted: string,
    id: string;
    jobDescription: string,
    jobExperience: number,
    jobMode: string,
    jobSalary: string,
    jobTitle: string,
    jobType: string,
    location: string,
    postedBy: string,
    rating: number,
}

export const initialStateJob = {
    companyLogo: "",
    companyName: "",
    datePosted: "",
    id: "",
    jobDescription: "",
    jobExperience: -1,
    jobMode: "",
    jobSalary: "",
    jobTitle: "",
    jobType: "",
    location: "",
    postedBy: "",
    rating: -1,
}