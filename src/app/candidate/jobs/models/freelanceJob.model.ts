import { FJob, initialStateFJob } from "./FJob.model";

export interface FreelanceJob {
    job: FJob,
    recruiter: string,
}

export const initialStateFreelanceJob = {
    job: initialStateFJob,
    recruiter: '',
}