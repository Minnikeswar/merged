
export interface LikesData{
    likes: Like[],
    username: string,
    __v: number;
    _id: string;
}

export interface Like{
    like_id: string;
    likedAt: string;
    _id: string;
}