import { SimilarJob } from "./similarJobs.model";

export interface ViewJob{
    companyLogo: string;
    description: string;
    employType: string;
    imageLifeAt: string;
    lifeAt: string;
    location: string;
    package: string;
    rating: number;
    similarJobs: SimilarJob[],
    skills: Skill[],
    title: string;
}

interface Skill{
    image_url: string;
    name: string;
}

