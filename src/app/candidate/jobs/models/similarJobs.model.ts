export interface SimilarJob{
    company_logo_url: string;
    employment_type: string;
    id: string;
    job_description: string;
    location: string;
    rating: number;
    title: string;
}


export const initialStateSimilar = {
    company_logo_url: "",
    employment_type: "",
    id: "",
    job_description: "",
    location: "",
    rating: 0,
    title: "",
}