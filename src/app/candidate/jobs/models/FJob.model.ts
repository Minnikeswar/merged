export interface FJob{
    companyName: string,
    datePosted: string,
    jobDescription: string,
    jobExperience: string,
    jobId: number,
    jobMode: string,
    jobSalary: string,
    jobScope: string,
    jobSkills: Array<string>,
    jobTitle: string,
    jobType: string,
    postedBy: string,
    salaryType: string,
    __v: number,
    _id:string,
}


export const initialStateFJob = {
    companyName: "",
    datePosted: "",
    jobDescription: "",
    jobExperience: "",
    jobId: -1,
    jobMode: "",
    jobSalary: "",
    jobScope: "",
    jobSkills: [],
    jobTitle: "",
    jobType: "",
    postedBy: "",
    salaryType: "",
    __v: -1,
    _id: "",
}