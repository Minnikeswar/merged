import { Pipe, PipeTransform } from "@angular/core";
import { Job } from "../models/job.model";


@Pipe({
    name: 'searchFilter'
})
export class FilterPipe implements PipeTransform{
    transform(jobs: Job[], searchText: string) {
        if(jobs.length === 0 || searchText === '')return jobs 
        const searchFor = searchText.toLowerCase()
        return jobs.filter((job) => {
            return job.companyName.toLowerCase().includes(searchFor) || job.jobDescription.toLowerCase().includes(searchFor) || job.jobTitle.toLowerCase().includes(searchFor)
        })
    }
}