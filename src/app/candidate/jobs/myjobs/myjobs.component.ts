import { Subscription, map } from 'rxjs';
import { Job } from './../models/job.model';
import { FetchService } from './../services/fetch.service';
import { Component, Input } from '@angular/core';
import { environment } from 'src/environments/environment.development';
import { Like, LikesData } from '../models/myjobs.model';



@Component({
  selector: 'app-myjobs',
  templateUrl: './myjobs.component.html',
  styleUrls: ['./myjobs.component.scss']
})
export class MyJobsComponent {
  allJobs: Job[] = [];
  p: number = 1;
  itemsPerPage: number = 5;
  totalProducts: number = 0;
  isLoading: boolean = true;
  textSearch: string = '';
  savedPosts: string[] = [];
  url: string = `${environment.likeUrl}`

  @Input() isSaved: boolean = false;

  initialSubscription:Subscription | null = null;
  updateSubscription: Subscription | null = null;

  constructor(private fetch: FetchService){

  }
  ngOnInit(){
      this.initialSubscription = this.fetch.getSaved().pipe(map((data: LikesData) => {
       return data['likes'] as Like[]
      })).subscribe((data:Like[]) => {
        this.savedPosts = data.map(each => each['like_id'])
      })

      this.updateSubscription = this.fetch.getUpdatedData().subscribe((data:string[]) => {
        if(!data) return;
        this.savedPosts = data 
      })      

      setTimeout(() => {
        this.allJobs = this.fetch.getJobsData()
        this.isLoading = false
        this.totalProducts = this.allJobs.length;
      },1000)
  }

  ngOnDestroy(){
    if(this.initialSubscription){
      this.initialSubscription.unsubscribe()
    }
    if(this.updateSubscription){
      this.updateSubscription.unsubscribe()
    }
  }

}
