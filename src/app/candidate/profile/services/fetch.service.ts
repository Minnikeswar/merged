import { environment } from 'src/environments/environment.development';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchService {

  constructor(private http: HttpClient) { }

  postProfile(username: string,data: any): Observable<any>{
    const postUrl = `${environment.postProfileUrl}/${username}`
    return this.http.post(postUrl,data)
  }
}
