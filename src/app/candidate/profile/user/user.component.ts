import { FetchService } from './../services/fetch.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Subscription, map } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {

  flag : boolean = true;

  //Added changes to code

  profileLoading: boolean = true;

  cur_user: string =  "";
  searched_for: string | null = "";

  profileForm: FormGroup;
  availableSkills = ['JavaScript', 'TypeScript', 'Python', 'Java', 'C++', 'HTML/CSS', 'Ruby', 'PHP', 'Go'];
  profileUser: string | null = JSON.parse(localStorage.getItem('cur_username') ?? '')
  disabled: boolean = false
  profileSubscription: Subscription | null = null;


  constructor(private fb: FormBuilder,private activeRoute: ActivatedRoute
    ,private http: HttpClient,
    private route: Router,
    private fetch: FetchService){
      this.profileForm = this.fb.group({
        username: [this.profileUser,Validators.required],
        firstname: ['',Validators.required],
        lastname: ['',Validators.required],
        email: ['',[Validators.required,Validators.email]],
        dob: ['',Validators.required],
        location: ['',Validators.required],
        phone: ['',Validators.required],
        company: ['',Validators.required],
        bio: ['',Validators.required],
        about: ['',[Validators.required,Validators.maxLength(300)]],
        // skills: this.fb.array([]),
        skills : new FormControl([]),
        github: ['',Validators.required],
        linkedin: ['',Validators.required],
      })
    }
  ngOnInit(): void{
    this.profileSubscription = this.profileForm?.valueChanges.subscribe((data:any) => {
      
    })


    const loggedInUser = localStorage.getItem('cur_username')
    if(loggedInUser){
      this.cur_user = JSON.parse(loggedInUser)
    }


    // this.cur_user = JSON.parse(localStorage.getItem('cur_username') ?? '')

    const params = this.activeRoute.paramMap.subscribe((params) => {

      this.profileUser = params.get('id')
      console.log('hi',this.profileUser)
      this.searched_for = this.profileUser
      if(this.profileUser !== null){
        this.disabled = true;
        let token = localStorage.getItem('cur_token')
        if(token){
            token = JSON.parse(token)
          
            const headers = new HttpHeaders({
              'authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
              'username': this.profileUser,
            }) 
  
            const url = 'http://localhost:7000/profile/'
            const obs$ = this.http.get(url+this.profileUser,{headers})
            obs$.pipe(map((data:any) => data.data)).subscribe((data) => {
              this.disabled = false;
              this.profileLoading = false;
              if(data.isNull === true){
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'User does not exist',
                }).then((res:any) => {
                  
                  if(res.isConfirmed){
                    this.route.navigateByUrl('/cand/jobs/all')
                  }
                })
                return;
              }
              if(!data)return;
              let date = data.dob.split('T')[0]
                  this.profileForm.patchValue({
                    username: this.profileUser,
                    firstname: data.firstName,
                    lastname: data.lastName,
                    email: data.email,
                    dob: date,
                    phone: data.phone,
                    location: data.location,
                    company: data.org,
                    bio: data.userBio,
                    about: data.userAbout,
                    github: data.github,
                    linkedin: data.linkedin,
                    skills : [...data.userSkills]
                  })
                  // data.userSkills.forEach((skill: string) => {
                  //   this.profileForm.get('skills').value.push(skill)
                  // });
                  this.profileLoading = false;
             })
        }

        }
      }
    )
    
  }
  
  addSkill(skill: string): void{
    if(this.profileForm.value.skills.includes(skill))return;
    this.profileForm.value.skills.push(skill)
  }

  removeSkill(index: number){
    this.profileForm.value.skills.splice(index,1)
  }

  onSubmit(): void{
    if(this.profileForm?.valid){
      const curProfile = this.profileForm.value
      const postObj = {
        username: curProfile.username,
        firstname: curProfile.firstname,
        lastname: curProfile.lastname,
        org: curProfile.company,
        email: curProfile.email,
        userSkills: curProfile.skills,
        userBio: curProfile.bio,
        dob: curProfile.dob,
        location: curProfile.location,
        phone: curProfile.phone,
        userAbout: curProfile.about,
        github: curProfile.github,
        linkedin: curProfile.linkedin,
      }

      const cur_user = this.profileForm.value.username
      this.fetch.postProfile(cur_user,postObj).subscribe((res)=>{
        if(res['message']){
          Swal.fire({
            position: "center",
            icon: "success",
            title: "Profile Updated Successfully",
            showConfirmButton: false,
            timer: 2000,
          });
        }
      })
      
    }
    else{
      console.log("Form is not valid")
    }
  }

  ngOnDestroy(){
    if(this.profileSubscription){
      this.profileSubscription.unsubscribe()
    }
  }
}
