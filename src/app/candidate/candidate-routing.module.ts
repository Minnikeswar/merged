import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CandidateComponent } from './candidate.component';
import { authGuard } from '../shared/guard/auth.guard';

const routes: Routes = [
   {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
   },
   {
    path: '',
    component: CandidateComponent,
    children: [
         
      {
        path: 'profile',
        loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
        canLoad: [authGuard]
       },
      {
        path: 'applications',
        loadChildren: () => import('./applications/applications.module').then(m => m.ApplicationsModule),
        canLoad: [authGuard]
      },
      {
        path: 'jobs',
        loadChildren: () => import('./jobs/jobs.module').then(m => m.JobsModule),
        canLoad: [authGuard]
      },
      {
        path: 'invitations',
        loadChildren: () => import('./invitations/invitations.module').then(m => m.InvitationsModule),
        canLoad: [authGuard]
      }
    ]
   },
   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CandidateRoutingModule { }
