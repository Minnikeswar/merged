import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { isLoggedOut } from './auth/guards/auth.guard';
import { isLoggedIn } from './recruiter/guards/recruiter.guard';
import { LandingComponent } from './shared/landing/landing.component';

const routes: Routes = [
  { path: '', component: AppComponent , children : [
    { path: '', component : LandingComponent},
    { path: 'rec', loadChildren: () => import('./recruiter/recruiter.module').then(m => m.RecruiterModule)},
    { path : 'cand' , loadChildren: () => import('./candidate/candidate.module').then(m => m.CandidateModule) },
    {path:"**" , component : PageNotFoundComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
