import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecruiterComponent } from './recruiter.component';
import { isLoggedIn } from './guards/recruiter.guard';
import { isLoggedOut } from '../auth/guards/auth.guard';

const routes: Routes = [
  { path : '' , component : RecruiterComponent , children : [
    {path : '' , redirectTo : 'auth' , pathMatch : 'full'},
    {path : 'auth' , loadChildren : () => import('../auth/auth.module').then(m => m.AuthModule) , canActivate : [isLoggedOut]},
    {path : 'job' , loadChildren : () => import('./job/job.module').then(m => m.JobModule) , canActivate : [isLoggedIn]},
    {path : 'people' , loadChildren : () => import('./people/people.module').then(m => m.PeopleModule) , canActivate : [isLoggedIn]},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecruiterRoutingModule { }
