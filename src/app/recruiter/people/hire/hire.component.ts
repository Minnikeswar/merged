import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { freelancersSelector } from '../store/people.selectors';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InviteModalComponent } from '../../components/invite-modal/invite-modal.component';
import { loadFreelancers } from '../store/people.actions';
import { InvitationsService } from '../store/invitations.service';
import { Subject, Subscription, exhaustMap, map, takeUntil } from 'rxjs';
import { AppCookieService } from '../../store/cookie.service';
import { Freelancer, FreelancerResponse, InvitationPayload, SendInvitationPayload, UserData } from '../../store/recruiter.interfaces';
import { User } from '../store/people.interface';

@Component({
  selector: 'app-hire',
  templateUrl: './hire.component.html',
  styleUrls: ['./hire.component.scss']
})
export class HireComponent implements OnInit {

  destroySubject = new Subject<void>();

  freelancers: Freelancer[] = [];
  allFreelancers: Freelancer[] = [];
  searchText1: string = '';
  searchText2: string = '';
  searchText3: string = '';
  inviter: UserData | null = null;
  constructor(private store: Store, private ngbModal: NgbModal, private invitationService: InvitationsService, private appCookieService: AppCookieService) {
    this.inviter = this.appCookieService.getUserData();
  }

  ngOnInit(): void {
    this.store.dispatch(loadFreelancers())
    this.store.select(freelancersSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((freelancers: Freelancer[]) => {
      this.allFreelancers = freelancers;
      this.freelancers = freelancers;
    });
  }

  searchProfiles() {
    this.freelancers = this.allFreelancers.filter((freelancer: Freelancer) => {
      return freelancer.username.toLowerCase().includes(this.searchText1.toLowerCase()) &&
        freelancer.userBio.toLowerCase().includes(this.searchText3.toLowerCase())
    });

    this.freelancers = this.freelancers.filter((freelancer: Freelancer) => {
      if (freelancer.userSkills.length == 0 && this.searchText2 === '') return true;
      for (let skill of freelancer.userSkills) {
        if (skill.toLowerCase().includes(this.searchText2.toLowerCase()))
          return true;
      }
      return false;
    })
  }

  sendInvite(freelancer: Freelancer) {
    const modalRef = this.ngbModal.open(InviteModalComponent, { size: 'lg', centered: true, backdrop: 'static', keyboard: false })
    modalRef.componentInstance.invitee = freelancer;
    modalRef.componentInstance.inviter = this.inviter;
    modalRef.componentInstance.submit.pipe(
      takeUntil(this.destroySubject),
      map((payload: InvitationPayload) => {
        const formData = payload.formData;
        const invitee = payload.invitee;
        formData.inviteeEmail = invitee.email;
        formData.inviteeUsername = invitee.username;
        const data: SendInvitationPayload = {
          inviteeId: freelancer._id,
          invitation: formData
        }
        return data;
      }),
      exhaustMap((data: SendInvitationPayload) => this.invitationService.sendInvitation(data))
    ).subscribe((response : string) => {
        if (response === 'SUCCESS') {
          modalRef.close();
        }
      });
  }

  ngOnDestroy() {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

}
