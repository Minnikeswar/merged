import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PeopleComponent } from './people.component';
import { HireComponent } from './hire/hire.component';
import { InvitationsComponent } from './invitations/invitations.component';
import { MyprofileComponent } from './myprofile/myprofile.component';

const routes: Routes = [
  {path:'' , component : PeopleComponent , children : [
    {path : 'hire' , component : HireComponent },
    {path : 'invitations' , component : InvitationsComponent },
    {path : 'myprofile' , component : MyprofileComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeopleRoutingModule { }
