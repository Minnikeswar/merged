import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../store/profile.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { emailValidator, passwordValidator } from '../../validators/form.validators';
import { Subject, Subscription, takeUntil } from 'rxjs';
import { ChangeEmailPayload, ChangePasswordPayload, User, userInitialState } from '../store/people.interface';
import { Store } from '@ngrx/store';
import { loadProfile, updateEmail, updateProfile } from '../store/people.actions';
import { userProfileSelector } from '../store/people.selectors';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.scss']
})
export class MyprofileComponent implements OnInit {

  destroySubject = new Subject<void>();

  profileData: User = userInitialState;

  profileForm: FormGroup;
  passwordForm: FormGroup;
  emailForm: FormGroup;

  constructor(private profileService: ProfileService, private store: Store) {

    this.emailForm = new FormGroup({
      email: new FormControl('', [Validators.required, emailValidator]),
      password: new FormControl('', [Validators.required, passwordValidator])
    })

    this.profileForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      companyName: new FormControl('', [Validators.required]),
    });

    this.passwordForm = new FormGroup({
      newPassword: new FormControl('', [Validators.required, passwordValidator]),
      confirmNewPassword: new FormControl('', [Validators.required, passwordValidator]),
      currentPassword: new FormControl('', [Validators.required, passwordValidator]),
    })
  }

  ngOnInit() {
    this.store.dispatch(loadProfile());

    this.store.select(userProfileSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((userProfile: User) => {
      this.profileData = userProfile;
      this.profileForm.setValue({
        firstName: this.profileData.firstName,
        lastName: this.profileData.lastName,
        companyName: this.profileData.companyName
      })

      this.emailForm.patchValue({
        email: this.profileData.email
      })
    })
  }

  updateEmail(): void {
    const payload: ChangeEmailPayload = this.emailForm.value;
    this.profileService.updateEmail(payload).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((response: string) => {
      if (!response) return;
      this.store.dispatch(updateEmail({ email: payload.email }));
    })
  }

  updateProfile(): void {
    const payload: User = this.profileForm.value;
    this.profileService.updateProfile(payload).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((response: string) => {
      if (!response) return;
      this.store.dispatch(updateProfile({ profile: payload }))
    })
  }

  updatePassword() : void {
    const payload : ChangePasswordPayload = this.passwordForm.value;
    this.profileService.changePassword(payload).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((response: string) => {
      if (!response) return;
      this.passwordForm.reset();
    })
  }

  ngOnDestroy() {
    this.destroySubject.next();
    this.destroySubject.complete();
  }
}
