import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { PeopleService } from "./people.service";
import { loadFreelancers, loadInvitations, loadProfile, setFreelancers, setInvitations, setProfile } from "./people.actions";
import { catchError, exhaustMap, finalize, map, of, switchMap, tap } from "rxjs";
import { RecruiterService } from "../../store/recruiter.service";
import { Store } from "@ngrx/store";
import { handleError } from "../../store/recruiter.actions";
import { ProfileService } from "./profile.service";
import { Freelancer, Invitation } from "../../store/recruiter.interfaces";
import { User } from "./people.interface";

@Injectable()
export class PeopleEffects {

    constructor(private actions$: Actions, private peopleService: PeopleService, private recruiterService: RecruiterService, private store: Store , private profileService : ProfileService) { }

    loadFreeLancers$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(loadFreelancers),
            exhaustMap(() => {
                this.recruiterService.setLoading(true);
                return this.peopleService.loadFreelancers().pipe(
                    map((freelancers : Freelancer[]) => {
                        return setFreelancers({ freelancers });
                    }),
                    catchError((err) => {
                        return of(handleError({ error: { error: 'Unable to fetch Freelancers' } }));
                    }),
                    finalize(() => this.recruiterService.setLoading(false))
                )
            })
        );
    })

    loadInvitations$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(loadInvitations),
            exhaustMap(() => {
                this.recruiterService.setLoading(true);
                return this.peopleService.loadInvitations().pipe(
                    map((invitations : Invitation[]) => {
                        return setInvitations({ invitations });
                    }),
                    catchError((err) => {
                        return of(handleError({ error: { error: 'Unable to fetch Invitations' } }));
                    }),
                    finalize(() => this.recruiterService.setLoading(false))
                )
            })
        )
    })

    loadProfile$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(loadProfile),
            exhaustMap(() => {
                this.recruiterService.setLoading(true);
                return this.profileService.getProfile().pipe(
                    map((profile : User) => {
                        return setProfile({ profile });
                    }),
                    finalize(() => this.recruiterService.setLoading(false))
                )
            })
        )
    });
}