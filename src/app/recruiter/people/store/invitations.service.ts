import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable, catchError, finalize, map, of, tap } from "rxjs";
import { handleError, handleSuccess } from "../../store/recruiter.actions";
import { RecruiterService } from "../../store/recruiter.service";
import { environment } from "src/environments/environment";
import { Invitation, SendInvitationPayload } from "../../store/recruiter.interfaces";

@Injectable({
    providedIn : 'root'
})

export class InvitationsService{
    constructor(private http : HttpClient , private store : Store , private recruiterService : RecruiterService) {}

    sendInvitation(payload : SendInvitationPayload) : Observable<string>{
        this.recruiterService.setLoading(true);
        return this.http.post(environment.backendUrl + "/hire/invite" , payload , {responseType : 'text'} ).pipe(
            map((response : string) => {
                this.store.dispatch(handleSuccess({success : {message : response}}));
                return 'SUCCESS';
            }),
            catchError((err) => {
                this.store.dispatch(handleError({error : err}))
                return of('ERROR');
            }),
            finalize(() => this.recruiterService.setLoading(false))
        );
    }

    revokeInvitation(invitationId : string) : Observable<string>{
        this.recruiterService.setLoading(true);
        return this.http.delete(environment.backendUrl + "/hire/deleteInvitation/" + invitationId  , {responseType : 'text'}).pipe(
            map((response : string) => {
                this.store.dispatch(handleSuccess({success : {message : response}}));
                return 'SUCCESS';
            }),
            catchError((err) => {
                this.store.dispatch(handleError({error : err}))
                return of('ERROR');
            }),
            finalize(() => this.recruiterService.setLoading(false))
        )
    }
}