import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { RecruiterService } from "../../store/recruiter.service";
import { catchError, finalize, map, tap } from "rxjs/operators";
import { EMPTY, Observable, of } from "rxjs";
import { Store } from "@ngrx/store";
import { handleError, handleSuccess } from "../../store/recruiter.actions";
import { environment } from "src/environments/environment";
import { ChangeEmailPayload, ChangePasswordPayload, User, userInitialState } from "./people.interface";

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    constructor(private http: HttpClient, private recruiterService: RecruiterService, private store: Store) { }

    getProfile(): Observable<User> {
        return this.http.get(environment.backendUrl + "/profile/getProfile").pipe(
            catchError((err) => {
                this.store.dispatch(handleError({ error: err }));
                return of(userInitialState);
            }),
            map((response) => {
                return response as User;
            }),
        )
    }

    updateEmail(data: ChangeEmailPayload) : Observable<string> {
        this.recruiterService.setLoading(true);
        return this.http.post(environment.backendUrl + "/profile/changeEmail", { data }, { responseType: 'text' }).pipe(
            map((response) => {
                this.store.dispatch(handleSuccess({ success: { message: response } }));
                return 'SUCCESS';
            }),
            catchError((err) => {
                this.store.dispatch(handleError({ error: err }));
                return of('');
            }),
            finalize(() => this.recruiterService.setLoading(false))
        )
    }

    updateProfile(profile: User) : Observable<string> {
        this.recruiterService.setLoading(true);
        return this.http.post(environment.backendUrl + "/profile/updateProfile", { profile }, { responseType: 'text' }).pipe(
            map((response) => {
                this.store.dispatch(handleSuccess({ success: { message: response } }));
                return 'SUCCESS';
            }),
            catchError((err) => {
                this.store.dispatch(handleError({ error: err }));
                return of('');
            }),
            finalize(() => this.recruiterService.setLoading(false))
        )
    }

    changePassword(data: ChangePasswordPayload) : Observable<string> {
        this.recruiterService.setLoading(true);
        return this.http.post(environment.backendUrl + "/profile/changePassword", { data }, { responseType: 'text' }).pipe(
            map((response) => {
                this.store.dispatch(handleSuccess({ success: { message: response } }));
                return 'SUCCESS';
            }),
            catchError((err) => {
                this.store.dispatch(handleError({ error: err }));
                return of('');
            }),
            finalize(() => this.recruiterService.setLoading(false))
        )
    }


}