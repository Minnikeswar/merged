
export interface User{
    companyName : string;
    email ?: string;
    firstName : string;
    lastName : string;
    username ?: string;
}

export const userInitialState : User ={
    companyName : '',
    firstName : '',
    lastName : '',
}

export interface ChangeEmailPayload{
    email : string;
    password : string;
}

export interface ChangePasswordPayload{
    currentPassword : string;
    newPassword : string;
    confirmNewPassword : string;
}