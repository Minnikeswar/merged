import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Freelancer, FreelancerResponse, Invitation, InvitationResponse } from "../../store/recruiter.interfaces";
import { User } from "./people.interface";

const featureSelector = createFeatureSelector<{freelancers : Freelancer[] , invitations : Invitation[] , userProfile : User}>('people');

export const freelancersSelector = createSelector(featureSelector, (state) => {
    return state.freelancers;
});

export const invitationsSelector = createSelector(featureSelector, (state) => {
    return state.invitations;
});

export const userProfileSelector = createSelector(featureSelector, (state) => {
    return state.userProfile;
});