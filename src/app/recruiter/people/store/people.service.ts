import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, map, tap } from "rxjs";
import { environment } from "src/environments/environment";
import { Freelancer, Invitation } from "../../store/recruiter.interfaces";

@Injectable({
    providedIn: 'root'
})

export class PeopleService {
    constructor(private http : HttpClient) { }

    loadFreelancers() : Observable<Freelancer[]>{
        return this.http.get(environment.backendUrl + '/hire/getFreelancers').pipe(
            map((data) => data as Freelancer[])
        )
    }

    loadInvitations() : Observable<Invitation[]>{
        return this.http.get(environment.backendUrl + '/hire/getInvitations').pipe(
            map((data) => data as Invitation[])
        )
    }
}