import { createAction, props } from "@ngrx/store";
import { Freelancer, Invitation } from "../../store/recruiter.interfaces";
import { User } from "./people.interface";

export const loadFreelancers = createAction('[People] Load Freelancers');

export const setFreelancers = createAction('[People] Set Freelancers', props<{freelancers: Freelancer[]}>());

export const loadInvitations = createAction('[People] Load Invitations');

export const setInvitations = createAction('[People] Set Invitations', props<{invitations: Invitation[]}>());

export const revokeInvitation = createAction('[People] Revoke Invitation', props<{invitationId: string}>());

export const loadProfile = createAction('[People] Load User Profile');

export const setProfile = createAction('[People] Set User Profile', props<{profile: User}>());

export const updateProfile = createAction('[People] Update User Profile', props<{profile: User}>());

export const updateEmail = createAction('[People] Update User Email', props<{email: string}>());