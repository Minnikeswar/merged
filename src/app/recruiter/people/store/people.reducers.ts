import { createReducer, on } from "@ngrx/store";
import { revokeInvitation, setFreelancers, setInvitations, setProfile, updateEmail, updateProfile } from "./people.actions";
import { Freelancer, Invitation } from "../../store/recruiter.interfaces";
import { handleLogout } from "src/app/auth/store/auth.actions";
import { User, userInitialState } from "./people.interface";

const invitations: Invitation[] = []
const freelancers: Freelancer[] = []
const profile : User = userInitialState;

export const freelancersReducer = createReducer(
    freelancers,
    on(setFreelancers, (state, {freelancers}) => {
        return freelancers
    } ), 
    on(handleLogout, (state) => [])
)

export const invitationsReducer = createReducer(
    invitations,
    on(setInvitations, (state, { invitations }) => {
        return invitations
    }),
    on(revokeInvitation , (state , { invitationId }) => state.filter(invitation => invitation._id !== invitationId)),
    on(handleLogout, (state) => [])
)

export const profileReducer = createReducer(
    profile,
    on(setProfile , (state , { profile }) => profile),
    on(handleLogout, (state) => userInitialState),
    on(updateEmail , (state , { email }) => ({...state , email})),
    on(updateProfile , (state , { profile }) => ({...state , ...profile})),
    on(handleLogout, (state) => userInitialState)
)