import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PeopleRoutingModule } from './people-routing.module';
import { HireComponent } from './hire/hire.component';
import { InvitationsComponent } from './invitations/invitations.component';
import { RecruiterModule } from '../recruiter.module';
import { PeopleComponent } from './people.component';
import { StoreModule } from '@ngrx/store';
import { freelancersReducer, invitationsReducer, profileReducer } from './store/people.reducers';
import { EffectsModule } from '@ngrx/effects';
import { PeopleEffects } from './store/people.effect';
import { MyprofileComponent } from './myprofile/myprofile.component';

@NgModule({
  declarations: [
    HireComponent,
    InvitationsComponent,
    PeopleComponent,
    MyprofileComponent
  ],
  imports: [
    CommonModule,
    PeopleRoutingModule,
    RecruiterModule, 
    StoreModule.forFeature('people',{freelancers : freelancersReducer , invitations : invitationsReducer , userProfile : profileReducer}),
    EffectsModule.forFeature([PeopleEffects])
  ],
})
export class PeopleModule { }
