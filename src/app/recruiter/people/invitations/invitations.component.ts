import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadInvitations, revokeInvitation } from '../store/people.actions';
import { invitationsSelector } from '../store/people.selectors';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewModalComponent } from '../../components/view-modal/view-modal.component';
import { InvitationsService } from '../store/invitations.service';
import { RecruiterService } from '../../store/recruiter.service';
import { Subject, Subscription, takeUntil } from 'rxjs';
import { Invitation, InvitationResponse } from '../../store/recruiter.interfaces';
import { environment } from 'src/environments/environment.development';

@Component({
  selector: 'app-invitations',
  templateUrl: './invitations.component.html',
  styleUrls: ['./invitations.component.scss']
})
export class InvitationsComponent implements OnInit {

  destroySubject = new Subject<void>();

  currTab = 'tab1';
  jobId: string = '';
  invitations: Invitation[] = [];
  approvedInvitations: Invitation[] = [];
  pendingInvitations: Invitation[] = [];
  rejectedInvitations: Invitation[] = [];

  INVITATION_STATUS = environment.INVITATION_STATUS;

  constructor(private store: Store , private ngbModal : NgbModal , private invitationService : InvitationsService , private recruiterService : RecruiterService) { }

  changeTab(tab: string) : void {
    this.currTab = tab;
  }

  partitionInvitations() : void{
    this.approvedInvitations = this.invitations.filter((invitation: Invitation) => invitation.status === this.INVITATION_STATUS.ACCEPTED);
    this.pendingInvitations = this.invitations.filter((invitation: Invitation) => invitation.status === this.INVITATION_STATUS.PENDING);
    this.rejectedInvitations = this.invitations.filter((invitation: Invitation) => invitation.status === this.INVITATION_STATUS.REJECTED);
  }

  ngOnInit(): void {
    this.store.dispatch(loadInvitations());
    this.store.select(invitationsSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((response : Invitation[]) => {
      if(!response) return;
      this.invitations = response;
      this.partitionInvitations();
    });
  }

  viewInvitaion(invitation : Invitation) : void{
    const modalRef = this.ngbModal.open(ViewModalComponent , {backdrop : 'static' , size :"lg" , centered : true , keyboard : false})
    modalRef.componentInstance.data = invitation;
    modalRef.componentInstance.title = "View Invitation";
  }

  revokeInvitation(invitation : Invitation) : void{
    this.recruiterService.showConfirmModal('Are you sure to revoke this invitation?' , 'Revoke').then((res) => {
      this.revoke(invitation);
    }).catch((err) => {});
  }


  revoke(invitation : Invitation) : void{
    this.invitationService.revokeInvitation(invitation._id).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((response) => {
      if(response === 'SUCCESS'){
        this.store.dispatch(revokeInvitation({invitationId : invitation._id}));
      }
    })
  }

  ngOnDestroy(){
    this.destroySubject.next();
    this.destroySubject.complete();
  }
}
