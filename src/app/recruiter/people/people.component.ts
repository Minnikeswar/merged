import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { loadFreelancers } from "./store/people.actions";
import { AppCookieService } from "../store/cookie.service";
import { RecruiterService } from "../store/recruiter.service";
import { handleLogout } from "src/app/auth/store/auth.actions";
import { Router } from "@angular/router";

@Component({
    selector: 'app-people',
    templateUrl: './people.component.html',
})
export class PeopleComponent{
    constructor( private recruiterService : RecruiterService) {}

    handleLogout() {
        this.recruiterService.showConfirmModal('Are you sure to logout?' , 'Logout').then((res) => {
          this.recruiterService.logout();
        }).catch((err) => {
          
        });
      }
}