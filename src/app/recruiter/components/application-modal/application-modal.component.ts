import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Application, dummyApplication } from '../../job/store/jobs.interfaces';

@Component({
  selector: 'app-application-modal',
  templateUrl: './application-modal.component.html',
  styleUrls: ['./application-modal.component.scss']
})
export class ApplicationModalComponent {
  @Input() application: Application = dummyApplication;
  constructor(private activeModal : NgbActiveModal) { }

  closeModal() {
    this.activeModal.dismiss();
  }
}
