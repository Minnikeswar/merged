import { Component, EventEmitter, Input, Output } from '@angular/core';
import { environment } from 'src/environments/environment.development';
import { Invitation, dummyInvitation } from '../../store/recruiter.interfaces';

@Component({
  selector: 'app-invitation-card',
  templateUrl: './invitation-card.component.html',
  styleUrls: ['./invitation-card.component.scss']
})

export class InvitationCardComponent {
  @Input() invitation: Invitation = dummyInvitation;
  @Output() view : EventEmitter<Invitation> = new EventEmitter();
  @Output() revoke : EventEmitter<Invitation> = new EventEmitter();
  @Output() invite : EventEmitter<Invitation> = new EventEmitter();

  INVITATION_STATUS = environment.INVITATION_STATUS;

  handleView() : void {
    this.view.emit(this.invitation);
  }

  handleRevoke() : void {
    this.revoke.emit(this.invitation);
  }

  handleInvite() : void {
    this.invite.emit(this.invitation);
  }
}
