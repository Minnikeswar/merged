import { Component, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { Input , Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Job, dummyJob } from '../../job/store/jobs.interfaces';

@Component({
  selector: 'app-job-modal',
  templateUrl: './job-modal.component.html',
  styleUrls: ['./job-modal.component.scss']
})
export class JobModalComponent{
  @Input() selectedJob: Job = dummyJob;

  constructor(private activeModal : NgbActiveModal , private activatedRoute : ActivatedRoute , private router : Router){

  }

  viewApplications() {
    this.activeModal.dismiss();
    this.router.navigate(['rec/job/applications/' + this.selectedJob._id]);
  }

  closeJobModal() {
    this.activeModal.dismiss();
  }

}
