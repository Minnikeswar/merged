import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Freelancer, dummyFreelancer } from '../../store/recruiter.interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent{
  @Input() profile : Freelancer = dummyFreelancer;
  @Output() invite : EventEmitter<Freelancer> = new EventEmitter<Freelancer>();

  constructor(private router : Router) { }

  handleInvite(){
    this.invite.emit(this.profile);
  }

  showProfile(){
    this.router.navigate(['../../../cand/profile/user' , this.profile.username])
  }
}
