import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Application } from '../../job/store/jobs.interfaces';
import { environment } from 'src/environments/environment.development';

@Component({
  selector: 'app-application-card',
  templateUrl: './application-card.component.html',
  styleUrls: ['./application-card.component.scss']
})
export class ApplicationCardComponent {
  APPLICATION_STATUS = environment.APPLICATION_STATUS;
  @Input() application: Application = {} as Application;
  @Output() approve : EventEmitter<Application> = new EventEmitter();
  @Output() reject : EventEmitter<Application> = new EventEmitter();
  @Output() view : EventEmitter<Application> = new EventEmitter();

  constructor() { }
  approveApplication() {
    this.approve.emit(this.application);
  }
  rejectApplication() {
    this.reject.emit(this.application);
  }
  viewApplication() {
    this.view.emit(this.application);
  }
}
