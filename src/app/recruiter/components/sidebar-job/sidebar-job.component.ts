import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Job, dummyJob } from '../../job/store/jobs.interfaces';

@Component({
  selector: 'app-sidebar-job',
  templateUrl: './sidebar-job.component.html',
  styleUrls: ['./sidebar-job.component.scss']
})
export class SidebarJobComponent {
  @Input() isActive: boolean = false;
  @Input() job : Job = dummyJob;
  @Output() handleSelectJob : EventEmitter<string> = new EventEmitter<string>();

  handleSelect(){
    this.handleSelectJob.emit(this.job._id);
  }
}
