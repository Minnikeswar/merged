import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarJobComponent } from './sidebar-job.component';

describe('SidebarJobComponent', () => {
  let component: SidebarJobComponent;
  let fixture: ComponentFixture<SidebarJobComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarJobComponent]
    });
    fixture = TestBed.createComponent(SidebarJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
