import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Freelancer, InvitationPayload, UserData, dummyFreelancer, dummyUserData } from '../../store/recruiter.interfaces';

@Component({
  selector: 'app-invite-modal',
  templateUrl: './invite-modal.component.html',
  styleUrls: ['./invite-modal.component.scss']
})
export class InviteModalComponent {
  @Input() invitee: Freelancer = dummyFreelancer;
  @Input() inviter: UserData = dummyUserData;
  @Output() submit: EventEmitter<InvitationPayload> = new EventEmitter<InvitationPayload>();

  jobScopes : string[] = ['Small', 'Medium', 'Large'];
  jobTypes : string[] = ['Part Time', 'Full Time', 'Performance based FTE'];
  jobModes : string[] = ['Work from Office', 'Work from Home', 'Hybrid'];
  salaryTypes : string[] = ['Hourly rate (/hr)', 'Fixed price'];

  invitationForm: FormGroup = new FormGroup({
    companyName: new FormControl('', Validators.required),
    jobDescription: new FormControl('', Validators.required),
    jobMode: new FormControl('Work from Office', Validators.required),
    jobSalary: new FormControl('', Validators.required),
    salaryType: new FormControl('Hourly rate (/hr)', Validators.required),
    jobScope: new FormControl('Small', Validators.required),
    jobTitle: new FormControl('', Validators.required),
    jobType: new FormControl('Part Time', Validators.required),
  });
  constructor(private activeModal: NgbActiveModal) { }

  handleClose(): void {
    this.activeModal.dismiss();
  }

  handleSubmit(): void {
    const payload : InvitationPayload= {
      formData: this.invitationForm.value,
      invitee: this.invitee,
    }
    this.submit.emit(payload);
  }
}
