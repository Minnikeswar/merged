import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Invitation, dummyInvitation } from '../../store/recruiter.interfaces';

@Component({
  selector: 'app-view-modal',
  templateUrl: './view-modal.component.html',
  styleUrls: ['./view-modal.component.scss']
})
export class ViewModalComponent {
  @Input() data : Invitation = dummyInvitation;
  @Input() title : string = '';
  constructor(private activeModal : NgbActiveModal) { }

  closeModal() : void {
    this.activeModal.close();
  }
}
