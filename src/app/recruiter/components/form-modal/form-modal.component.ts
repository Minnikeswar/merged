import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JobFormData } from '../../job/store/jobs.interfaces';

@Component({
  selector: 'app-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss']
})
export class FormModalComponent{
  @Output() addJob : EventEmitter<JobFormData> = new EventEmitter<JobFormData>();
  addJobForm : FormGroup = new FormGroup({
    companyName : new FormControl('' , Validators.required),
    jobDescription : new FormControl('' , Validators.required),
    jobMode : new FormControl('Work from Office'  , Validators.required),
    jobSalary : new FormControl('' , Validators.required),
    salaryType : new FormControl('Hourly rate (/hr)'  , Validators.required),
    jobScope : new FormControl('Small' , Validators.required),
    jobExperience : new FormControl('Fresher (0-2 years)'  , Validators.required),
    jobSkills: new FormControl<string[] | null>([]), 
    jobTitle: new FormControl('' , Validators.required),
    jobType: new FormControl('Part Time'  , Validators.required),
  });

  constructor(private activeModal : NgbActiveModal){
   
  }
  
  handleClose() : void{
    this.activeModal.dismiss();
  }

  handleSubmit() : void{
    const formData : JobFormData = this.addJobForm.value;
    this.addJob.emit(formData);
  }

}
