import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent {
  @Input() title: string = '';
  @Input() action : string = '';

  constructor(private ngbactiveModal : NgbActiveModal){}

  close(){
    this.ngbactiveModal.dismiss();
  }

  submit(){
    this.ngbactiveModal.close(true);
  }
}
