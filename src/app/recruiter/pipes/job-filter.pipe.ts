import { Pipe, PipeTransform } from '@angular/core';
import { Job } from '../job/store/jobs.interfaces';

@Pipe({
  name: 'jobFilter'
})
export class JobFilterPipe implements PipeTransform {

  transform(jobs: Job[], text: string): Job[] {
    return jobs.filter((job) => job.jobTitle.toLowerCase().includes(text.toLowerCase()));
  }

}
