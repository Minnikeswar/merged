import { createAction, props } from "@ngrx/store";
import { ErrorResponse, SuccessResponse } from "./recruiter.interfaces";

export const setLoading = createAction('[Recruiter] Set Loading' , props<{loading: boolean}>());

export const handleError = createAction('[Recruiter] Handle Error' , props<{error: ErrorResponse}>());

export const handleSuccess = createAction('[Recruiter] Handle Success' , props<{success: SuccessResponse}>());

// export const handleLogoutRec = createAction('[Recruiter] Handle Logout');