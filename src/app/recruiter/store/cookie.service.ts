import { Injectable } from "@angular/core";
import { CookieService } from 'ngx-cookie-service';
import { UserData } from "./recruiter.interfaces";

@Injectable({
    providedIn: "root"
})
export class AppCookieService {
    constructor(private cookieService : CookieService) {}

    getExpireTime() : Date{
        const currTime = new Date().getTime();
        const tokenExpiry = new Date(currTime + 1000 * 60 * 60 * 10);
        return tokenExpiry;
    }

    clearAll() : void{
        this.cookieService.deleteAll('/');
    }

    getJwtToken() : string{
        return this.cookieService.get('jwt');
    }

    setUserData(data : UserData) : void{
        const tokenExpiry = this.getExpireTime();
        this.cookieService.set('userData', JSON.stringify(data) , {expires : tokenExpiry , path: '/'});
    }

    getUserData() : UserData | null{
        if(!this.cookieService.check('userData')) return null;
        return JSON.parse(this.cookieService.get('userData'));
    }

    deleteUserData() : void{
        this.cookieService.delete('userData');
    }

    setJwtToken(token : string) : void{
        const tokenExpiry = this.getExpireTime();
        this.cookieService.set('jwt', token , {expires : tokenExpiry , path: '/'});
    }

    deleteJwtToken() : void{
        this.cookieService.delete('jwt');
    }
}