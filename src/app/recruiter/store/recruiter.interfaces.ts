export interface Invitation{
    _id: string;
    invitee : string;
    inviter : string;
    inviteeUsername : string;
    inviterUsername : string;
    inviteeEmail : string;
    companyName : string;
    jobTitle : string;
    jobType : string;
    jobMode : string;
    jobScope : string;
    salaryType : string;
    jobSalary : string;
    status : number;
    createdAt : string;
    jobDescription : string;
}

export const dummyInvitation : Invitation = {
    _id: '',
    invitee : '',
    inviter : '',
    inviteeUsername : '',
    inviterUsername : '',
    inviteeEmail : '',
    companyName : '',
    jobTitle : '',
    jobType : '',
    jobMode : '',
    jobScope : '',
    salaryType : '',
    jobSalary : '',
    status : 0,
    createdAt : "",
    jobDescription : '',
}

export interface InvitationResponse{
    invitations : Invitation[];
}

interface InvitationFormData{
    companyName : string;
    jobDescription : string;
    jobMode : string;
    jobSalary : string;
    salaryType : string;
    jobScope : string;
    jobTitle : string;
    jobType : string;
    inviteeEmail ?: string;
    inviteeUsername ?: string;
}

export interface InvitationPayload{
    formData : InvitationFormData;
    invitee : Freelancer;
}

export interface SendInvitationPayload{
    inviteeId : string;
    invitation : InvitationFormData;
}

export interface FreelancerResponse{
    freelancers : Freelancer[];
}

export interface Freelancer{
    _id: string;
    username : string;
    email : string;
    firstName : string;
    lastName : string;
    userSkills : string[];
    userAbout : string;
    userBio : string;
    dob ?: string;
    github ?: string;
    linkedin ?: string;
    location ?: string;
    org ?: string;
    phone ?: string;
    profileUrl ?: string;
}

export const dummyFreelancer : Freelancer = {
    _id: '',
    username : '',
    email : '',
    firstName : '',
    lastName : '',
    userSkills : [],
    userAbout : '',
    userBio : '',
}

export interface UserData{
    id: string;
    email : string;
    exp : number;
    iat : number;
    isRecruiter : boolean;
    loginTime : string;
    username : string;
    userStatus : string;
}

export const dummyUserData : UserData = {
    id: '',
    email : '',
    exp : 0,
    iat : 0,
    isRecruiter : true,
    loginTime : '',
    username : '',
    userStatus : '',
}

export interface ErrorResponse{
    error : string;
    status ?: number;
}

export interface SuccessResponse{
    message : string;
}

export const dummyErrorResponse : ErrorResponse = {
    error : '',
}

export const dummySuccessResponse : SuccessResponse = {
    message : '',
}

export interface RecruiterState{
    loading : number;
    error : ErrorResponse;
    success : SuccessResponse;
}