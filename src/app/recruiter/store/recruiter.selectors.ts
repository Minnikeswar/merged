import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RecruiterState } from './recruiter.interfaces';

const recruiterSelector = createFeatureSelector<RecruiterState>('recruiter');

export const loadingSelector = createSelector(recruiterSelector, (state) => state.loading);

export const errorSelector = createSelector(recruiterSelector, (state) => state.error);

export const successSelector = createSelector(recruiterSelector, (state) => state.success);
