import { createReducer, on } from "@ngrx/store";
import { handleError, setLoading , handleSuccess } from "./recruiter.actions";
import { handleLogout } from "src/app/auth/store/auth.actions";
import { ErrorResponse, SuccessResponse, dummyErrorResponse, dummySuccessResponse } from "./recruiter.interfaces";

export const loading : number = 0;
export const success : SuccessResponse = dummySuccessResponse;
export const error : ErrorResponse = dummyErrorResponse;

export const loadingReducer = createReducer(
    loading,
    on(setLoading , (state , {loading}) => {
        if(loading) return state + 1;
        return state - 1;
    }),
    on(handleLogout , state => 0)
)

export const errorReducer = createReducer(
    error,
    on(handleError , (state , {error}) => {
        return error;
    }),
    on(handleLogout , state => dummyErrorResponse)
)

export const successReducer = createReducer(
    success,
    on(handleSuccess , (state , {success}) => {
        return success
    }),
    on(handleLogout , state => dummySuccessResponse)
)