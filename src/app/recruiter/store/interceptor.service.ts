import { HttpInterceptor , HttpRequest , HttpHandler , HttpEvent } from "@angular/common/http";
import { Observable, catchError, of, throwError } from "rxjs";
import { AppCookieService } from "./cookie.service";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { handleLogout } from "src/app/auth/store/auth.actions";
import { RecruiterService } from "./recruiter.service";

@Injectable()
export class InterceptorService implements HttpInterceptor{

  constructor(private appCookieService : AppCookieService ,private store : Store, private router : Router , private recruiterService : RecruiterService ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(!req.url.includes('8000') || req.url.includes('auth')){
      return next.handle(req);
    }
    const token = this.appCookieService.getJwtToken();
    const newReq = req.clone({
      headers: req.headers.set('Authorization', `Bearer ${token}`)
    })
    return next.handle(newReq).pipe(
      catchError((error) => {
        if(error.status === 401){
          this.recruiterService.logout();
        }
        throw error;
      })
    )
  }
}