import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { setLoading } from "./recruiter.actions";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ConfirmModalComponent } from "../components/confirm-modal/confirm-modal.component";
import { AppCookieService } from "./cookie.service";
import { handleLogout } from "src/app/auth/store/auth.actions";
import { Router } from "@angular/router";


@Injectable({
    providedIn: 'root'
})
export class RecruiterService {
    constructor(private store : Store , private ngbModal : NgbModal , private appCookieService : AppCookieService , private router : Router){
        
    }

    setLoading(loading : boolean) : void{
        this.store.dispatch(setLoading({loading : loading}));
    }

    showConfirmModal(title : string, action : string){
        const modalRef = this.ngbModal.open(ConfirmModalComponent, {centered : true , backdrop : 'static' , keyboard : false});
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.action = action;
        return modalRef.result;
    }

    logout() : void{
        this.appCookieService.clearAll();
        this.store.dispatch(handleLogout());
        this.router.navigate(['/']);
    }
}