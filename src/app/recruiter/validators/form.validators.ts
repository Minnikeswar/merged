import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function passwordValidator(control: AbstractControl): ValidationErrors | null {

    const value : string = control.value?.trim();

    if(!value) return {required: true};

    if(value.length < 8) return {minlength: true};

    const hasUpperCase = /[A-Z]+/.test(value);

    const hasLowerCase = /[a-z]+/.test(value);

    const hasNumeric = /[0-9]+/.test(value);

    const hasSpace = /\s+/.test(value);

    const hasSpecialCharacters = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(value);

    const passwordValid = hasUpperCase && hasLowerCase && hasNumeric && hasSpecialCharacters && !hasSpace;

    return !passwordValid ? { invalid: true } : null;
}

export function textValidator(control: AbstractControl): ValidationErrors | null {

    const value : string = control.value.trim();

    if(!value) return {required: true};

    const validText = /^[a-zA-Z0-9\s]*$/.test(value);

    return !validText ? { invalid: true } : null;
}

export function emailValidator(control: AbstractControl): ValidationErrors | null {

    const value : string = control.value?.trim();

    if(!value) return {required: true};

    const validEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(value);

    return !validEmail ? { email: true } : null;
}