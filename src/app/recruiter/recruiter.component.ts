import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { MessageService } from "primeng/api";
import { errorSelector, loadingSelector, successSelector } from "./store/recruiter.selectors";
import { Subject, Subscription, delay, map, takeUntil } from "rxjs";
import { AppCookieService } from "./store/cookie.service";
import { Router } from "@angular/router";
import { handleLogout } from "../auth/store/auth.actions";
import { RecruiterService } from "./store/recruiter.service";
import { ErrorResponse, SuccessResponse, dummyErrorResponse, dummySuccessResponse } from "./store/recruiter.interfaces";

@Component({
  selector: 'app-recruiter',
  templateUrl: './recruiter.component.html',
  providers: []
})

export class RecruiterComponent implements OnInit {
  isLoading$ = this.store.select(loadingSelector).pipe(delay(0));

  destroySubject = new Subject<void>();

  constructor(private store: Store, private messageService: MessageService, private appCookieService: AppCookieService, private router: Router , private recruiterService : RecruiterService) {

  }

  ngOnInit() {
    this.store.select(errorSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((error : ErrorResponse) => {
      if(error === dummyErrorResponse) return;
      this.messageService.clear();
      this.messageService.add({ severity: 'error', summary: 'Error', detail: error.status === 404 ? 'Not Found' : error.error });
    })
    
    this.store.select(successSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((success : SuccessResponse) => {
      if(success === dummySuccessResponse) return;
      this.messageService.clear();
      this.messageService.add({ severity: 'success', summary: 'Success', detail: success.message });
    });
  }

  ngOnDestroy(){
    this.destroySubject.next();
    this.destroySubject.complete();
  }
}