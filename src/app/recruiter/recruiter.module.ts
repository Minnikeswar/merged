import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecruiterRoutingModule } from './recruiter-routing.module';
import { RecruiterComponent } from './recruiter.component';
import { SharedModule } from '../shared/shared.module';
import { ProfileListComponent } from './components/profile-list/profile-list.component';
import { TagComponent } from './components/tag/tag.component';
import { FormModalComponent } from './components/form-modal/form-modal.component';
import { JobModalComponent } from './components/job-modal/job-modal.component';
import { StoreModule } from '@ngrx/store';
import { errorReducer, loadingReducer, successReducer } from './store/recruiter.reducers';
import { ChipsModule } from 'primeng/chips';
import { ApplicationCardComponent } from './components/application-card/application-card.component';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ApplicationModalComponent } from './components/application-modal/application-modal.component';
import { InviteModalComponent } from './components/invite-modal/invite-modal.component';
import { InvitationCardComponent } from './components/invitation-card/invitation-card.component';
import { ViewModalComponent } from './components/view-modal/view-modal.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmModalComponent } from './components/confirm-modal/confirm-modal.component';
import { SidebarJobComponent } from './components/sidebar-job/sidebar-job.component';
import { JobFilterPipe } from './pipes/job-filter.pipe';
import { ToastModule } from 'primeng/toast';


@NgModule({
  declarations: [
    RecruiterComponent,
    ProfileListComponent,
    TagComponent,
    FormModalComponent,
    JobModalComponent,
    ApplicationCardComponent,
    ApplicationModalComponent,
    InviteModalComponent,
    InvitationCardComponent,
    ViewModalComponent,
    ConfirmModalComponent,
    SidebarJobComponent,
    JobFilterPipe
  ],
  imports: [
    CommonModule,
    ChipsModule,
    RecruiterRoutingModule,
    SharedModule,
    StoreModule.forFeature('recruiter', {loading: loadingReducer , error : errorReducer , success : successReducer}),
    ConfirmDialogModule,
    ToastModule
  ],
  exports: [
    SharedModule,
    ProfileListComponent,
    InvitationCardComponent,
    TagComponent,
    FormModalComponent,
    JobModalComponent,
    ChipsModule,
    ApplicationCardComponent,
    ApplicationModalComponent,
    InviteModalComponent,
    JobModalComponent,
    NgbModule,
    SidebarJobComponent,
    JobFilterPipe
  ],
  providers: [NgbActiveModal ]
})
export class RecruiterModule { }
