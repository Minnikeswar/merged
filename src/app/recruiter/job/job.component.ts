import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { loadJobs } from "./store/jobs.actions";
import { AppCookieService } from "../store/cookie.service";
import { RecruiterService } from "../store/recruiter.service";
import { handleLogout } from "src/app/auth/store/auth.actions";
import { Router } from "@angular/router";


@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
})

export class JobComponent implements OnInit {
  constructor(private store: Store, private recruiterService: RecruiterService, private appCookieService: AppCookieService, private router: Router) {

  }

  handleLogout() {
    this.recruiterService.showConfirmModal('Are you sure to logout?', 'Logout').then((res) => {
      this.recruiterService.logout();
    }).catch((err) => {

    });
  }


  ngOnInit() {
    this.store.dispatch(loadJobs());
  }
}