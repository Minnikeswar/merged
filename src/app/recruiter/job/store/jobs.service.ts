import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, catchError, finalize, map, of, tap } from 'rxjs';
import { handleError, handleSuccess, setLoading } from '../../store/recruiter.actions';
import { RecruiterService } from '../../store/recruiter.service';
import { environment } from 'src/environments/environment';
import { Freelancer } from '../../store/recruiter.interfaces';
import { Job, JobFormData } from './jobs.interfaces';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private http : HttpClient , private store : Store , private recuiterService : RecruiterService) { }

  //it is being called using effects, so error handling is done in effects and loading too
  getMatchedProfiles(jobId : string) : Observable<Freelancer[]>{
    return this.http.get(environment.backendUrl + '/home/similarProfiles/' + jobId).pipe(
      catchError((err) => {
        this.store.dispatch(handleError({error : err}));
        return of([]);
      }),
      map((data) => data as Freelancer[])
    )
  }

  //it is being called using effects, so error handling is done in effects and loading too
  getAllJobs() : Observable<Job[]> {
    return this.http.get(environment.backendUrl + '/job/myjobs' ).pipe(
      catchError((err) => {
        this.store.dispatch(handleError({error : err}));
        return of([]);
      }),
      map((data) => data as Job[])
    )
  }

  deleteJob(jobId : string) : Observable<string> {
    this.recuiterService.setLoading(true);
    return this.http.delete(environment.backendUrl + '/job/delete/' + jobId , {responseType : 'text'}).pipe(
      map((res : string) => {
        this.store.dispatch(handleSuccess({success : {message : res}}))
        return 'SUCCESS'
      }),
      catchError((err) => {
        this.store.dispatch(handleError({error : err}));
        return of('ERROR');
      }),
      finalize(() => this.recuiterService.setLoading(false))
    )
  }

  addJob(job : JobFormData) : Observable<string> {
    return this.http.post(environment.backendUrl + '/job/add' , {newJob : job} , {responseType : 'text'}).pipe(
      map((res : string) => {
        this.store.dispatch(handleSuccess({success : {message : res}}));
        return 'SUCCESS';
      }),
      catchError((err) => {
          this.store.dispatch(handleError({error : err}));
          return of('ERROR');
        })
      )
  }
  
}
