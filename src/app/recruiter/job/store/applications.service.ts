import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, catchError, finalize, map, of, tap } from "rxjs";
import { RecruiterService } from "../../store/recruiter.service";
import { Store } from "@ngrx/store";
import { handleError, handleSuccess } from "../../store/recruiter.actions";
import { environment } from "src/environments/environment";
import { Application } from "./jobs.interfaces";

@Injectable({
    providedIn: 'root'
})
export class ApplicationsService {
    constructor(private http : HttpClient , private recruiterService : RecruiterService , private store :  Store) { }

    getApplications(jobId : string) : Observable<Application[]> {
        return this.http.get(environment.backendUrl + '/job/getApplications/' + jobId).pipe(
          catchError((err) => {
            this.store.dispatch(handleError({error : err}));
            return of([]);
          }),
          map((data) => data as Application[]),
        )
      }

    rejectApplication(applicationId : string) : Observable<string> {
        this.recruiterService.setLoading(true);
        return this.http.post(environment.backendUrl + '/job/rejectApplication/' , {applicationId} , {responseType : 'text'} ).pipe(
            map((res) => {
                this.store.dispatch(handleSuccess({success : {message : res}}));
                return 'SUCCESS'
            }),
            catchError((err) => {
                this.store.dispatch(handleError({error : err}));
                return of('ERROR');
            }),
            finalize(() => this.recruiterService.setLoading(false))
        )
    }

    approveApplication(applicationId : string) : Observable<string>{
        this.recruiterService.setLoading(true);
        return this.http.post(environment.backendUrl + '/job/approveApplication/' , {applicationId} , {responseType : 'text'}).pipe(
            map((res) => {
                this.store.dispatch(handleSuccess({success : {message : res}}));
                return 'SUCCESS'
            }),
            catchError((err) => {
                this.store.dispatch(handleError({error : err}));
                return of('ERROR');
            }),
            finalize(() => this.recruiterService.setLoading(false))
        )
    }
}