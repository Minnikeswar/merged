import { Injectable } from "@angular/core";
import { Actions, ofType, createEffect } from "@ngrx/effects";
import { exhaustMap, finalize, map} from "rxjs/operators";
import { fetchApplications, fetchMatchingProfiles, loadJobs , setApplications, setJobs, setMatchingProfiles } from "./jobs.actions";
import { JobService } from "./jobs.service";
import { RecruiterService } from "../../store/recruiter.service";
import { Freelancer } from "../../store/recruiter.interfaces";
import { Application, Job } from "./jobs.interfaces";
import { ApplicationsService } from "./applications.service";


@Injectable()
export class JobEffects {
  constructor(private actions$: Actions, private jobService : JobService , private recruiterService : RecruiterService , private applicationService : ApplicationsService) {}

  loadJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loadJobs),
      exhaustMap(() => {
        this.recruiterService.setLoading(true);
        return this.jobService.getAllJobs().pipe(
          map((jobs : Job[]) => {
            return setJobs({jobs});
          }),
          finalize(() => this.recruiterService.setLoading(false))
        )
      })
    )
  })

  fetchMatchingProfiles$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(fetchMatchingProfiles),
      exhaustMap((action) => {
        this.recruiterService.setLoading(true);
        return this.jobService.getMatchedProfiles(action.jobId).pipe(
          map((profiles : Freelancer[]) => {
            return setMatchingProfiles({matchingProfiles : profiles});
          }),
          finalize(() => this.recruiterService.setLoading(false))
        )
      })
    )
  })

  fetchApplications$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(fetchApplications),
      exhaustMap((action) => {
        this.recruiterService.setLoading(true);
        return this.applicationService.getApplications(action.jobId).pipe(
          map((applications : Application[]) => {
            return setApplications({applications});
          }),
          finalize(() => this.recruiterService.setLoading(false))
        )
      })
    )
  })

}