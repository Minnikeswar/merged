import { createFeatureSelector, createSelector } from '@ngrx/store';
import { JobStates } from './jobs.interfaces';

const jobSelector = createFeatureSelector<JobStates>('job');

export const jobsSelector = createSelector(jobSelector, (state) => state.jobs);

export const matchingProfilesSelector = createSelector(jobSelector, (state) => state.matchingProfiles);

export const approvedApplicationsSelector = createSelector(jobSelector, (state) => state.approvedApplications);

export const pendingApplicationsSelector = createSelector(jobSelector, (state) => state.pendingApplications);

export const rejectedApplicationsSelector = createSelector(jobSelector, (state) => state.rejectedApplications);