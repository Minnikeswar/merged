import { Freelancer, dummyFreelancer } from "../../store/recruiter.interfaces";

export interface FetchMatchingProfilesPayload {
    jobId: string;
}

export interface JobStates{
    jobs: Job[];
    matchingProfiles: Freelancer[];
    approvedApplications: Application[];
    pendingApplications: Application[];
    rejectedApplications: Application[];
}

export interface Job{
    _id: string;
    companyName: string;
    jobTitle: string;
    datePosted : string;
    jobId : number;
    jobMode : string;
    jobType : string;
    postedBy : string;
    jobSalary : string;
    salaryType : string;
    jobDescription : string;
    jobSkills : string[];
    jobExperience : string;
    jobScope : string;
}

export const dummyJob : Job = {
    _id: '',
    companyName: '',
    jobTitle: '',
    datePosted : '',
    jobId : 0,
    jobMode : '',
    jobType : '',
    postedBy : '',
    jobSalary : '',
    salaryType : '',
    jobDescription : '',
    jobSkills : [],
    jobExperience : '',
    jobScope : '',
}

export interface JobFormData{
    companyName: string;
    jobTitle: string;
    jobMode : string;
    jobType : string;
    jobSalary : string;
    salaryType : string;
    jobDescription : string;
    jobSkills : string[];
    jobExperience : string;
    jobScope : string;
}

export interface Application{
    appliedAt : string;
    jobId: string;
    recruiterId: string;
    startDate: string;
    status: number;
    _id: string;
    appliedBy : Freelancer;
}

export const dummyApplication : Application = {
    appliedAt : '',
    jobId: '',
    recruiterId: '',
    startDate: '',
    status: 0,
    _id: '',
    appliedBy : dummyFreelancer
}

export interface ApplicationQueryParams{
    jobId : string;
}