import { createAction, props } from '@ngrx/store';
import { Application, FetchMatchingProfilesPayload, Job } from './jobs.interfaces';
import { Freelancer } from '../../store/recruiter.interfaces';

export const loadJobs = createAction('[Recruiter] Load Jobs');

export const setJobs = createAction('[Recruiter] Set Jobs' , props<{jobs: Job[]}>());

export const addJob = createAction('[Recruiter] Add Job' , props<{job: Job}>());

export const deleteJob = createAction('[Recruiter] Delete Job' , props<{jobId: string}>());

export const fetchApplications = createAction('[Recruiter] Fetch Applications' , props<{jobId: string}>());

export const setApplications = createAction('[Recruiter] Set Applications' , props<{applications: Application[]}>());

export const approveApplication = createAction('[Recruiter] Approve Application' , props<{application: Application}>());

export const rejectApplication = createAction('[Recruiter] Reject Application' , props<{application: Application}>());

export const fetchMatchingProfiles = createAction('[Recruiter] Fetch Matching Profiles' , props<FetchMatchingProfilesPayload>());

export const setMatchingProfiles = createAction('[Recruiter] Set Matching Profiles' , props<{matchingProfiles : Freelancer[]}>());

export const clearMatchingProfiles = createAction('[Recruiter] Clear Matching Profiles');

export const nullifyApplications = createAction('[Recruiter] Nullify Applications');

export const nullifyJobs = createAction('[Recruiter] Nullify Jobs');

export const nullifyMatchingProfiles = createAction('[Recruiter] Nullify Matching Profiles');