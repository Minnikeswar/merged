import { createReducer, on } from "@ngrx/store";
import { approveApplication, deleteJob, nullifyApplications, nullifyMatchingProfiles, rejectApplication, setApplications, setJobs, setMatchingProfiles } from "./jobs.actions";
import { Freelancer } from "../../store/recruiter.interfaces";
import { Application, Job } from "./jobs.interfaces";
import { environment } from "src/environments/environment";
import { handleLogout } from "src/app/auth/store/auth.actions";

export const jobs : Job[] = [];
export const matchingProfiles : Freelancer[] = [];
export const approvedApplications : Application[] = [];
export const pendingApplications : Application[] = [];
export const rejectedApplications : Application[] = [];

const APPLICATION_STATUS = environment.APPLICATION_STATUS;

export const jobsReducer = createReducer(
    jobs,
    on(setJobs , (state , {jobs}) => jobs),
    on(deleteJob , (state , {jobId}) => state.filter(job => job._id !== jobId)),
    on(handleLogout , () => [])
)

export const matchingProfilesReducer = createReducer(
    matchingProfiles,
    on(setMatchingProfiles , (state , {matchingProfiles}) => matchingProfiles),
    on(handleLogout , () => []),
    on(nullifyMatchingProfiles , () => [])
);

export const approvedApplicationsReducer = createReducer(
    approvedApplications,
    on(setApplications , (state , {applications}) =>{
        return applications.filter(app => app.status === APPLICATION_STATUS.ACCEPTED)
    }),
    on(approveApplication , (state , {application}) => {
        const app = {...application , status : APPLICATION_STATUS.ACCEPTED};
        return [...state , app];
    }),
    on(handleLogout , () => []),
    on(nullifyApplications , () => [])
);

export const pendingApplicationsReducer = createReducer(
    pendingApplications,
    on(setApplications , (state , {applications}) => {
        return applications.filter(app => app.status === APPLICATION_STATUS.PENDING)
    }),
    on(approveApplication , (state , {application}) => state.filter(app => app._id !== application._id)),
    on(rejectApplication , (state , {application}) => state.filter(app => app._id !== application._id)),
    on(handleLogout , () => []),
    on(nullifyApplications , () => [])
);

export const rejectedApplicationsReducer = createReducer(
    rejectedApplications,
    on(setApplications , (state , {applications}) => {
        return applications.filter(app => app.status === APPLICATION_STATUS.REJECTED)
    }),
    on(rejectApplication , (state , {application}) => {
        const app = {...application , status : APPLICATION_STATUS.REJECTED};
        return [...state , app];
    }),
    on(handleLogout , () => []),
    on(nullifyApplications , () => [])
);


