import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { JobService } from '../store/jobs.service';
import { jobsSelector } from '../store/jobs.selectors';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JobModalComponent } from '../../components/job-modal/job-modal.component';
import { FormModalComponent } from '../../components/form-modal/form-modal.component';
import { deleteJob, loadJobs } from '../store/jobs.actions';
import { RecruiterService } from '../../store/recruiter.service';
import { Subject, Subscription, exhaustMap, takeUntil } from 'rxjs';
import { Job, JobFormData } from '../store/jobs.interfaces';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {

  destroySubject = new Subject<void>();

  selectedJob: Job | null = null;
  jobs: Job[] = [];
  allJobs: Job[] = [];
  searchText: string = '';
  sortTypes: string[] = ['Date latest', 'Date oldest', 'Role ascending', 'Role descending'];
  selectedSort: string = 'Date latest';

  constructor(private jobService: JobService, private recruiterService: RecruiterService, private store: Store, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.store.select(jobsSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((res: Job[]) => {
      this.jobs = [...res];
      this.allJobs = [...res];
      this.sortJobs();
    });
  }

  viewJob(job: Job): void {
    this.selectedJob = job;
    const modalRef = this.modalService.open(JobModalComponent, { size: 'lg', backdrop: 'static', centered: true });
    modalRef.componentInstance.selectedJob = this.selectedJob;
  }

  deleteJob(job: Job): void {
    this.recruiterService.showConfirmModal('Are you sure to delete this job?', 'Delete').then((res) => {
      this.delete(job);
    }).catch((err) => {

    });
  }

  delete(job: Job): void {
    this.jobService.deleteJob(job._id).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((res) => {
      if (res === 'ERROR') return;
      this.store.dispatch(deleteJob({ jobId: job._id }));
    });
  }

  setSortType(sortType: string): void {
    this.selectedSort = sortType;
    this.sortJobs();
  }

  sortJobs(): void {
    if (this.selectedSort === 'Date latest') {
      this.jobs.sort((a: Job, b: Job) => {
        return new Date(b.datePosted).getTime() - new Date(a.datePosted).getTime();
      });
    }
    else if (this.selectedSort === 'Date oldest') {
      this.jobs.sort((a: Job, b: Job) => {
        return new Date(a.datePosted).getTime() - new Date(b.datePosted).getTime();
      });
    }
    else if (this.selectedSort === 'Role ascending') {
      this.jobs.sort((a: Job, b: Job) => {
        return a.jobTitle.localeCompare(b.jobTitle);
      });
    }
    else if (this.selectedSort === 'Role descending') {
      this.jobs.sort((a: Job, b: Job) => {
        return b.jobTitle.localeCompare(a.jobTitle);
      });
    }
  }

  filterJobs(): void {
    this.jobs = this.allJobs.filter((job: Job) => {
      return job.jobTitle.toLowerCase().includes(this.searchText.toLowerCase());
    });
  }

  handleShowAddJobModal(): void {
    const modalRef = this.modalService.open(FormModalComponent, { size: 'lg', backdrop: 'static', keyboard: false, centered: true })
    modalRef.componentInstance.addJob.pipe(
      takeUntil(this.destroySubject),
      exhaustMap((data: JobFormData) => this.jobService.addJob(data))
    ).subscribe((res: string) => {
      if (res === 'SUCCESS') {
        modalRef.close();
        this.store.dispatch(loadJobs());
      }
    });
  }

  ngOnDestroy() {
    this.destroySubject.next();
    this.destroySubject.complete();
  }
}
