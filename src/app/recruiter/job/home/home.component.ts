import { Component, OnInit } from '@angular/core';
import { FormGroup , FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subject, Subscription, exhaustMap, map, takeUntil } from 'rxjs';
import { JobService } from '../store/jobs.service';
import { jobsSelector, matchingProfilesSelector } from '../store/jobs.selectors';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InviteModalComponent } from '../../components/invite-modal/invite-modal.component';
import { InvitationsService } from '../../people/store/invitations.service';
import { AppCookieService } from '../../store/cookie.service';
import { Freelancer, InvitationPayload, SendInvitationPayload, UserData } from '../../store/recruiter.interfaces';
import { fetchMatchingProfiles, nullifyMatchingProfiles } from '../store/jobs.actions';
import { FetchMatchingProfilesPayload, Job } from '../store/jobs.interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit{

  destroySubject = new Subject<void>();

  matchingProfiles : Freelancer[] = [];
  jobs : Job[] = [];
  inviter : UserData | null = null;
  selectedJobId : string = "";
  filterText : string = "";
  imageUrl : string = "../../../../assets/images/Select_a_job.png";

  constructor( private jobService : JobService , private store : Store , private ngbModal : NgbModal , private invitationService : InvitationsService , private appCookieService : AppCookieService){
    this.inviter = this.appCookieService.getUserData();
  }

  ngOnInit() {
    this.store.select(jobsSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((data : Job[]) => {
      this.jobs = [...data];
    });

    this.store.select(matchingProfilesSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((profiles : Freelancer[]) => {
      this.matchingProfiles = [...profiles];
    })
  }

  selectJob(jobId : string){
    this.selectedJobId = jobId;
    if(jobId){
      const payload : FetchMatchingProfilesPayload = {
        jobId
      }
      this.store.dispatch(fetchMatchingProfiles(payload))
    }
  }

  sendInvite(freelancer : Freelancer){
    const modalRef = this.ngbModal.open(InviteModalComponent , {size : 'lg' , centered : true , backdrop : 'static' , keyboard : false})
    modalRef.componentInstance.invitee = freelancer;
    modalRef.componentInstance.inviter = this.inviter;
    modalRef.componentInstance.submit.pipe(
      takeUntil(this.destroySubject),
      map((payload : InvitationPayload) => {
        const formData  = payload.formData;
        const invitee = payload.invitee;
        formData.inviteeEmail = invitee.email;
        formData.inviteeUsername = invitee.username;
        const data : SendInvitationPayload = {
          inviteeId : freelancer._id,
          invitation : formData
        }
        return data;
      }),
      exhaustMap((data : SendInvitationPayload) => this.invitationService.sendInvitation(data))
    ).subscribe((response : string) => {
        if(response === 'SUCCESS'){
          modalRef.close();
        }
      });
  }

  ngOnDestroy(){
    this.destroySubject.next();
    this.destroySubject.complete();

    this.store.dispatch(nullifyMatchingProfiles());
  }
}
