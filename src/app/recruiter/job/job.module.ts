import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobRoutingModule } from './job-routing.module';
import { RecruiterModule } from '../recruiter.module';
import { JobComponent } from './job.component';
import { JobsComponent } from './jobs/jobs.component';
import { HomeComponent } from './home/home.component';
import { StoreModule } from '@ngrx/store';
import { approvedApplicationsReducer, jobsReducer, matchingProfilesReducer, pendingApplicationsReducer, rejectedApplicationsReducer } from './store/jobs.reducers';
import { EffectsModule } from '@ngrx/effects';
import { JobEffects } from './store/jobs.effects';
import { ApplicationsComponent } from './applications/applications.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    JobComponent,
    JobsComponent,
    HomeComponent,
    ApplicationsComponent
  ],
  imports: [
    CommonModule,
    JobRoutingModule,
    RecruiterModule,
    StoreModule.forFeature('job', { jobs : jobsReducer , matchingProfiles : matchingProfilesReducer , approvedApplications : approvedApplicationsReducer , pendingApplications : pendingApplicationsReducer , rejectedApplications : rejectedApplicationsReducer}),
    EffectsModule.forFeature([JobEffects]),
    NgbDropdownModule
  ],
  exports:[
  ]
})
export class JobModule { }
