import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobComponent } from './job.component';
import { JobsComponent } from './jobs/jobs.component';
import { HomeComponent } from './home/home.component';
import { ApplicationsComponent } from './applications/applications.component';
import { isLoggedIn } from '../guards/recruiter.guard';

const routes: Routes = [
  {path: '' , component: JobComponent , children: [
    {path: '' , redirectTo: 'home' , pathMatch: 'full'},
    {path: 'home' , component : HomeComponent },
    {path: 'myjobs' , component : JobsComponent },
    {path: 'applications/:jobId' , component : ApplicationsComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobRoutingModule { }
