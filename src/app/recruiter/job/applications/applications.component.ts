import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApplicationsService } from '../store/applications.service';
import { RecruiterService } from '../../store/recruiter.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApplicationModalComponent } from '../../components/application-modal/application-modal.component';
import { Subject, Subscription, takeUntil } from 'rxjs';
import { Application, ApplicationQueryParams } from '../store/jobs.interfaces';
import { Store } from '@ngrx/store';
import { approvedApplicationsSelector, pendingApplicationsSelector, rejectedApplicationsSelector } from '../store/jobs.selectors';
import { approveApplication, fetchApplications, nullifyApplications, rejectApplication } from '../store/jobs.actions';
import { environment } from 'src/environments/environment.development';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements OnInit {

  destroySubject = new Subject<void>();
  
  currTab : string = 'tab1';
  jobId: string = '';
  approvedApplications: Application[] = [];
  pendingApplications: Application[] = [];
  rejectedApplications: Application[] = [];

  APPLICATION_STATUS = environment.APPLICATION_STATUS;
  constructor(private activeRoute: ActivatedRoute, private recruiterService : RecruiterService, private applicationService: ApplicationsService , private modalService : NgbModal , private store : Store) {
    this.activeRoute.params.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((params) => {
      this.jobId = params['jobId'];
      this.store.dispatch(fetchApplications({jobId : this.jobId}))
    });
  }

  ngOnInit(): void {
    this.store.select(approvedApplicationsSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((applications : Application[]) => {
      if(!applications) return;
      this.approvedApplications = applications;
    });

    this.store.select(pendingApplicationsSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((applications : Application[]) => {
      if(!applications) return;
      this.pendingApplications = applications;
    });

    this.store.select(rejectedApplicationsSelector).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((applications : Application[]) => {
      if(!applications) return;
      this.rejectedApplications = applications;
    });
  }

  viewApplication(application: Application) {
    const modalRef = this.modalService.open(ApplicationModalComponent , {size : 'lg' , centered : true , backdrop : 'static'});
    modalRef.componentInstance.application = application;
  }

  approveApplication(application: Application) {
    this.applicationService.approveApplication(application._id).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((res) => {
      if (res === 'SUCCESS') {
        this.store.dispatch(approveApplication({application}));
      }
    });
  }

  rejectApplication(application: Application) {
    this.recruiterService.showConfirmModal('Are you sure to reject this application?' , 'Reject').then((res) => {
      this.reject(application);
    }).catch((err) => {});
  }

  reject(application: Application) {
    this.applicationService.rejectApplication(application._id).pipe(
      takeUntil(this.destroySubject)
    ).subscribe((res) => {
      if (res === 'SUCCESS') {
        this.store.dispatch(rejectApplication({application}));
      }
    });
  }
  
  changeTab(tab: string) {
    this.currTab = tab;
  }

  ngOnDestroy(){
    this.destroySubject.next();
    this.destroySubject.complete();

    this.store.dispatch(nullifyApplications())
  }
}
