import { inject } from "@angular/core";
import { CanActivateFn, Router } from "@angular/router";
import { AppCookieService } from "../store/cookie.service";
import { MessageService } from "primeng/api";

export const isLoggedIn : CanActivateFn = (route , state) => {
    const appCookieService = inject(AppCookieService);
    const messageService = inject(MessageService);
    const router = inject(Router);
    const token = appCookieService.getJwtToken();
    const userData = appCookieService.getUserData();
    if(!token || !userData || !userData.isRecruiter){
        if(!token || !userData){
            messageService.add({severity:'warn', summary:'Warning', detail:'Please login to continue'});
        }
        else{
            messageService.add({severity:'warn', summary:'Warning', detail:'You are not authorized to access this page'});
        }
        router.navigate(['/']);
        return false;
    }
    return true;
}