import { Directive, ElementRef } from '@angular/core';
import { Subscription, fromEvent } from 'rxjs';

@Directive({
  selector: '[appLeadingSpace]'
})
export class LeadingSpaceDirective {

  subscription : Subscription | null = null;

  constructor(private element : ElementRef) { 
    this.subscription = fromEvent(this.element.nativeElement , 'input').subscribe((event : any) => {
      if(event.target.value === null) return;
      event.target.value = event.target.value.trimLeft();
    });
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

}
