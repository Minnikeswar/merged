import { Directive, ElementRef } from '@angular/core';
import { Subscription, fromEvent } from 'rxjs';

@Directive({
  selector: '[appNoSpace]'
})
export class NoSpaceDirective {

  inputSubscription : Subscription | null = null;

  constructor(private element : ElementRef) { 
    this.inputSubscription = fromEvent(this.element.nativeElement , 'input').subscribe((event : any) => {
      if(event.target.value === null) return;
      event.target.value = event.target.value.trim();
    });
  }

  ngOnDestroy() {
    this.inputSubscription?.unsubscribe();
  }

}
