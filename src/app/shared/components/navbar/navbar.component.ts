import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  @Output() logout : EventEmitter<null> = new EventEmitter();

  constructor(private router : Router ) { }

  handleLogout(){
    this.logout.emit();
  }
}
