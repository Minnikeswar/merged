import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoaderModalComponent } from '../loader-modal/loader-modal.component';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnChanges{
  @Input() showLoader : number | null= 0;
  constructor(private ngbModal : NgbModal) { 
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.showLoader === null) return;
    if(this.showLoader > 0){
      this.ngbModal.open(LoaderModalComponent , {backdrop : 'static' , size : "sm" , centered : true , keyboard : false})
    }
    else{
      this.ngbModal.dismissAll();
    }
  }
}
