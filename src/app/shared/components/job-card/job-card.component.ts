import { Component, Output } from '@angular/core';
import { Input } from '@angular/core';
import { Job } from 'src/app/recruiter/job/store/jobs.interfaces';

@Component({
  selector: 'app-job-card',
  templateUrl: './job-card.component.html',
  styleUrls: ['./job-card.component.scss']
})
export class JobCardComponent {
  @Input() job! : Job;
  constructor(){
    
  }
}