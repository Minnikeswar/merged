import { Component } from '@angular/core';
import { MdbModalRef } from 'mdb-angular-ui-kit/modal';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

  jobTitle: string = ""
  jobDescription: string = "";
  jobExperience: string = "";
  jobMode: string = "";
  jobType: string = "";
  jobScope: string = "";
  jobSkills: string[] = [];
  salaryType: string = "";
  jobSalary: string = "";
  companyName: string = "";
  postedBy: string = "";

  constructor(public modalRef: MdbModalRef<ModalComponent>){}
}
