
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";


export class AuthInterceptor implements HttpInterceptor{

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if(!req.url.includes('7000') || req.url.includes('auth')){
            return next.handle(req)
        }
        // console.log('Auth Interceptor called');
        const checkToken = localStorage.getItem('cur_token')
        if(checkToken){
            if(req.url.startsWith('https://apis.ccbp.in')){
                return next.handle(req)
            }
            const token = JSON.parse(checkToken)
            const cloneReq = req.clone({
                headers: req.headers.set('Authorization',`Bearer ${token}`)
            })
            // console.log('Interceptor called')
            return next.handle(cloneReq)
        }
        
        return next.handle(req)
    }
}