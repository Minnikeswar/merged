import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GetTokenService {


  jwtToken(){
    const token = localStorage.getItem('cur_token')
    if(token){
      return JSON.parse(token)
    }
    return null
  }
}
