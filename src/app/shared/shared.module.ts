import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { JobCardComponent } from './components/job-card/job-card.component';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { AppModule } from '../app.module';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderModalComponent } from './components/loader-modal/loader-modal.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { JobTitleFormatPipe } from './pipes/job-title-format.pipe';
import { NoSpaceDirective } from '../directives/trailing-space.directive';
import { LandingComponent } from './landing/landing.component';
import { MainloaderComponent } from './mainloader/mainloader.component';
import { CommonloaderComponent } from './commonloader.component';
import { MyNavbarComponent } from './mynavbar/navbar.component';
import { SavebuttonComponent } from './savebutton/savebutton.component';
import { ModalComponent } from './modal/modal.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { GetTokenService } from './services/get-token.service';
import { LeadingSpaceDirective } from '../directives/leading-space.directive';
// import { TrimspacesDirective } from '../candidate/directives/trimspaces.directive';


@NgModule({
  declarations: [
    NavbarComponent,
    JobCardComponent,
    DateFormatPipe,
    LoaderComponent,
    LoaderModalComponent,
    PageNotFoundComponent,
    JobTitleFormatPipe,
    NoSpaceDirective,
    LeadingSpaceDirective,

    LandingComponent,
    MainloaderComponent,
    CommonloaderComponent,
    MyNavbarComponent,
    SavebuttonComponent,
    ModalComponent,
    NotFoundComponent,
    // TrimspacesDirective

  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
    HttpClientModule,
  ],
  exports: [
    NavbarComponent,
    JobCardComponent,
    DateFormatPipe,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
    HttpClientModule,
    LoaderComponent,
    JobTitleFormatPipe,
    NoSpaceDirective,
    LeadingSpaceDirective,

    LandingComponent,
    MainloaderComponent,
    CommonloaderComponent,
    MyNavbarComponent,
    SavebuttonComponent,
    ModalComponent,
    NotFoundComponent,
    // TrimspacesDirective
  ],
  providers: [GetTokenService]
})
export class SharedModule { }
