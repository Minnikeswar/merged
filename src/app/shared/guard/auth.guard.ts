import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { GetTokenService } from '../services/get-token.service';

export const authGuard: CanActivateFn = (route, state) => {
  const router = inject(Router)
  const tokenService = inject(GetTokenService)
  const token = tokenService.jwtToken()
  if(!token || token === 'undefined'){
    router.navigateByUrl('/cand/auth/login')
    return false;
  }
  return true;
};
