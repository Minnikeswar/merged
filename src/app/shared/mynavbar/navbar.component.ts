import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import Swal from 'sweetalert2';
import { GetTokenService } from '../services/get-token.service';

@Component({
  selector: 'my-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class MyNavbarComponent {

  username: string | null = null;

  constructor(private route: Router,private store: Store,private tokenService: GetTokenService){}

  ngOnInit(){
    const user_name = localStorage.getItem('cur_username')
    if(user_name){
      this.username = JSON.parse(user_name)
    }
  }

  onLogout(){
    Swal.fire({
      title: "Are you sure?",
      text: "Do want to Logout from this application?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, Logout!"
    }).then((result) => {
      if (result.isConfirmed) {
        localStorage.removeItem('cur_token')
        localStorage.removeItem('cur_username')
        this.route.navigateByUrl('/')
      }
    });
     
  }

  viewProfile(){
    this.route.navigateByUrl(`/cand/profile/user/${this.username}`)
  }
}
