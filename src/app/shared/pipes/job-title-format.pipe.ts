import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jobTitleFormat'
})
export class JobTitleFormatPipe implements PipeTransform {

  transform(jobTitle: string, companyName : string , postedDate : string): string {
    return `${jobTitle} at ${companyName} posted on ${postedDate}`
  }

}
