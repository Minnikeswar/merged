import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastModule } from 'primeng/toast';
import { ConfirmationService, MessageService, SharedModule } from 'primeng/api';
import { Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InterceptorService } from './recruiter/store/interceptor.service';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { MdbModalService } from 'mdb-angular-ui-kit/modal';
import { AuthInterceptor } from './shared/services/authInterceptor.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToastModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: !isDevMode(),
      autoPause: true,
    }),
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    SharedModule
  ],
  exports: [
    SharedModule,
  ],
  providers: [MessageService , ConfirmationService ,
    {
      provide : HTTP_INTERCEPTORS , useClass : AuthInterceptor , multi : true
    },
     {
    provide : HTTP_INTERCEPTORS , useClass : InterceptorService , multi : true
  } , MdbModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
